export default {
  entry: 'src/js/main.js',
  dest: 'build/main.min.js',
  format: 'iife',
  sourceMap: 'inline',
};