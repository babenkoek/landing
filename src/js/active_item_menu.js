//active item menu
const li = document.getElementsByTagName('li');
const item = [];
for (let i = 0; i < li.length; i++) {
    item[i] = li[i].querySelector('a');
}

const activeMenu = (index) => {
	for (let i = li.length - 1; i >= 0; i--)
		item[i].classList.remove('active-item');
    item[index].classList.add('active-item');               
};

for (let i = li.length - 1; i >= 0; i--) {
    item[i].addEventListener('click', function() {
        activeMenu(i);
    });
}