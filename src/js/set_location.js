//set location window
import {array} from "./scrolling_active_item";

const optionsLocation = {
	root: document.querySelector('#parallax-main-content'),
	rootMargin: '0px',
	threshold: 1.0
};

const setLocation = (url) => {
	location.hash = url;
};

const handleIntersectionLocation = (entries, observerLocation) => {
	entries.forEach((entery) => {
		if (entery.intersectionRatio > 0) {
			setLocation(entery.target.id);
		}
	});
};

if ('IntersectionObserver' in window) {
	const observerLocation = new IntersectionObserver(handleIntersectionLocation, optionsLocation);
	array.forEach(section => {
		observerLocation.observe(section);
	});	
} else {
	//parallaxContainer.addEventListener('scroll', isVisibleSection, false);
}