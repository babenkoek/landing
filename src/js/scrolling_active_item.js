//scrolling section -> switching menu item
import {goToTop} from './go_to_top';

const optionsSection = {
	root: document.querySelector('#parallax-main-content'),
	rootMargin: '0px',
	threshold: 0.51
};

const array = [];
array.push(document.getElementById('main-screen'));
array.push(document.getElementById('advantages'));
array.push(document.getElementById('team'));
array.push(document.getElementById('projects'));
array.push(document.getElementById('form'));


const li = document.getElementsByTagName('li');
const item = [];
for (let i = 0; i < li.length; i++) {
    item[i] = li[i].querySelector('a');
}

const activeMenu = (index) => {
	for (let i = li.length - 1; i >= 0; i--)
		item[i].classList.remove('active-item');
    item[index].classList.add('active-item');               
};


const handleIntersectionSection = (entries, observerSection) => {
	entries.forEach((entery) => {
		if (entery.intersectionRatio > 0) {
			activeMenu(array.indexOf(entery.target) + 1);
			//if (entery.target.id === 'advantages') 
			//	goToTop();
		};
	});
};

if ('IntersectionObserver' in window) {
	const observerSection = new IntersectionObserver(handleIntersectionSection, optionsSection);
	array.forEach(section => {
		observerSection.observe(section);
	});	
} else {
	//parallaxContainer.addEventListener('scroll', isVisibleSection, false);
}

export {array};

