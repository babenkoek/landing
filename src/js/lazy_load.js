const images = document.querySelectorAll('.lazy-load');
const options = {
	root: document.querySelector('#parallax-main-content'),
	rootMargin: '0px',
	threshold: 0.1
};

const fetchImage = (url) => {
	return new Promise((resolve, reject) => {
		const image = new Image();
		image.src = url;
		image.onload = resolve;
		image.onerror = reject;
	});
};

const loadImage = (image) => {
	const src = image.dataset.src;
	fetchImage(src).then(() => {
		image.src = src;
	});
};

const handleIntersection = (entries, observer) => {
	entries.forEach(entery => {
		if (entery.intersectionRatio > 0) {
			loadImage(entery.target);
		}
	});
};

if ('IntersectionObserver' in window) {
	const observer = new IntersectionObserver(handleIntersection, options);
	images.forEach(img => {
		observer.observe(img);
	});
} else {
	Array.from(images).forEach(image => loadImage(image));
}

// lazy-load for background

const divBckgr = document.querySelectorAll('.lazy-load-bckgr');

const loadImageBckgr = (div) => {
	let src = div.dataset.url;
	fetchImage(src).then(() => {
		div.style.backgroundImage = `url(${src})`;
	});
};

const handleIntersectionBckgr = (entries, observer) => {
	entries.forEach(entery => {
		if (entery.intersectionRatio > 0) {
			loadImageBckgr(entery.target);
			observer.unobserve(entery.target);
		}
	});
};

if ('IntersectionObserver' in window) {
	const observerBckgr = new IntersectionObserver(handleIntersectionBckgr, options);
	divBckgr.forEach(div => {
		observerBckgr.observe(div);
	});
} else {
	Array.from(divBckgr).forEach(div => loadImageBckgr(div));
}

//load image for #main-screen

Modernizr.on('webp', result => {
  if (result) {
    const block = document.getElementById('main-screen');
    block.classList.add('webp');

  } else {
    const block = document.getElementById('main-screen');
    block.classList.add('no-webp');
  }
});