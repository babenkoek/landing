//go to top
const goToTop = () => {
	const button = document.createElement('button');
	const textNode = '<img src="img/arrow_up.svg" alt="вверх" style="width: 24px;">';
	button.innerHTML = textNode;
	button.className = 'mdl-button mdl-js-button mdl-button--icon go-to-top';
	componentHandler.upgradeElement(button);
	document.getElementById('parallax-main-content').appendChild(button);
};

export {goToTop}