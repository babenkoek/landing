const ul = document.getElementsByTagName('ul');
const li = document.getElementsByTagName('li');
const item = [];
for (let i = 0; i < li.length; i++) {
    item[i] = li[i].querySelector('a');
}
const minMenu = document.getElementById('menu-toggle');
const header = document.getElementsByTagName('header');
const parallaxContainer = document.getElementById('parallax-main-content');


/*
function elmYPosition(eID) {
    var elm = document.getElementById(eID);
    var y = elm.offsetTop;
    var node = elm;
    while (node.offsetParent && node.offsetParent != document.body) {
        node = node.offsetParent;
        y += node.offsetTop;
    } return y;
}

//animate SVG 
let isVisibleSVG = function() {
    const topElPosition = elmYPosition('column_svg') + 200;
    const winPosition = parallaxContainer.scrollTop + document.documentElement.clientHeight;

    if (topElPosition < winPosition) {
        const divSvg1 = document.getElementById('div_svg1');
        divSvg1.innerHTML = '<svg id="svg1" class="svg-image" version="1.1" id="wer" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="-244 46 470 470" xml:space="preserve"><g><path stroke-dasharray="1963" stroke-dashoffset="0" d="M203.9,52.5h-409.1c-9.2,0-16.8,7.5-16.8,16.8v310.1c0,9.2,7.5,16.8,16.8,16.8h154.3v54.1h-66.6c-3.9,0-7.1,3.2-7.1,7.1c0,3.9,3.2,7.1,7.1,7.1h233.8c3.9,0,7.1-3.2,7.1-7.1c0-3.9-3.2-7.1-7.1-7.1H49.6v-54.1h154.3c9.2,0,16.8-7.5,16.8-16.8V69.3C220.7,60.1,213.1,52.5,203.9,52.5z"><animate attributeName="stroke-dashoffset" begin="0s" values="1963; 0" dur="3s" repeatCount="1" fill="freeze" calcMode="linear"/><animate attributeName="stroke-dashoffset" begin="svg1.mouseover" values="1963; 0" dur="3s" repeatCount="1" fill="freeze" calcMode="linear"/></path><path stroke-dasharray="325" stroke-dashoffset="0" stroke="#AABF19" d="M-122.8,301h37.7c3.9,0,7.1-3.2,7.1-7.1V191.5c0-3.9-3.2-7.1-7.1-7.1h-37.7c-3.9,0-7.1,3.2-7.1,7.1v102.5C-129.9,297.9-126.7,301-122.8,301z"><animate attributeName="stroke-dashoffset" begin="0s" values="325; 0" dur="3s" repeatCount="1" fill="freeze" calcMode="linear"/><animate attributeName="stroke-dashoffset" begin="svg1.mouseover" values="325; 0" dur="3s" repeatCount="1" fill="freeze" calcMode="linear"/></path><path stroke-dasharray="392" stroke-dashoffset="0" stroke="#AABF19" d="M-56.9,301h37.7c3.9,0,7.1-3.2,7.1-7.1V158c0-3.9-3.2-7.1-7.1-7.1h-37.7c-3.9,0-7.1,3.2-7.1,7.1v135.9C-64,297.9-60.8,301-56.9,301z"><animate attributeName="stroke-dashoffset" begin="0s" values="392; 0" dur="3s" repeatCount="1" fill="freeze" calcMode="linear"/><animate attributeName="stroke-dashoffset" begin="svg1.mouseover" values="392; 0" dur="3s" repeatCount="1" fill="freeze" calcMode="linear"/></path><path stroke-dasharray="455" stroke-dashoffset="0" stroke="#AABF19" d="M9,301h37.7c3.9,0,7.1-3.2,7.1-7.1V126.5c0-3.9-3.2-7.1-7.1-7.1H9c-3.9,0-7.1,3.2-7.1,7.1v167.5C2,297.9,5.2,301,9,301z"><animate attributeName="stroke-dashoffset" begin="0s" values="455; 0" dur="3s" repeatCount="1" fill="freeze" calcMode="linear"/><animate attributeName="stroke-dashoffset" begin="svg1.mouseover" values="455; 0" dur="3s" repeatCount="1" fill="freeze" calcMode="linear"/></path><path stroke-dasharray="260" stroke-dashoffset="0" stroke="#AABF19" d="M75,301h37.7c3.9,0,7.1-3.2,7.1-7.1v-70c0-3.9-3.2-7.1-7.1-7.1H75c-3.9,0-7.1,3.2-7.1,7.1v70C67.9,297.9,71.1,301,75,301z"><animate attributeName="stroke-dashoffset" begin="0s" values="260; 0" dur="3s" repeatCount="1" fill="freeze" calcMode="linear"/><animate attributeName="stroke-dashoffset" begin="svg1.mouseover" values="260; 0" dur="3s" repeatCount="1" fill="freeze" calcMode="linear"/></path></g><line stroke-dasharray="325" stroke-dashoffset="0" x1="-167.6" y1="343" x2="157.4" y2="343"><animate attributeName="stroke-dashoffset" begin="0s" values="325; 0" dur="3s" repeatCount="1" fill="freeze" calcMode="linear"/><animate attributeName="stroke-dashoffset" begin="svg1.mouseover" values="325; 0" dur="3s" repeatCount="1" fill="freeze" calcMode="linear"/></line></svg>';
        const divSvg2 = document.getElementById('div_svg2');
        divSvg2.innerHTML = '<svg id="svg2" class="svg-image" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"viewBox="-86 -113.5 786.5 786.5" xml:space="preserve"><g><path stroke-dasharray="1223" stroke-dashoffset="0" stroke="#AABF19" d="M480,435.7L480,435.7L480,435.7c-0.2-1.6-0.6-3.2-1.1-4.7c-3.8-14.6-12.5-41.3-16.5-53.2c-0.4-2.1-1.1-4.1-2-6c0,0-6.4-9.6-11.2-12.5c0,0-2.4-1.3-2.7-1.4c-9.5-4.5-38.9-16.3-38.9-16.3l0,0c-14.7-6.4-29.9-11.8-45.4-17.3l0,0l0,0l0,0l0,0c-2.8,11.5-29.1,88.1-33.3,98.8l-3.3-71.2c1.1-1.7,1.9-3.6,2.8-5.4l11-21.7c-7.7,6.4-19.3,10.5-32.3,10.5c-12.7,0-24-3.9-31.7-10.1l10.7,21.2c0.9,1.8,1.7,3.8,2.8,5.4l-3.3,71.2c-4.2-10.6-30.5-87.2-33.3-98.8l0,0l0,0l0,0l0,0c-15.5,5.5-30.7,11-45.4,17.3l0,0c0,0-29.4,11.8-38.9,16.3c-0.3,0.2-2.4,1.2-2.7,1.4c0,0-8.8,7.3-11.2,12.4c0,0-1.6,3.9-2,6c-3.9,11.9-12.6,38.5-16.5,53.2c-0.5,1.5-0.9,3.1-1.1,4.7l0,0l0,0c-0.2,1.4-0.3,2.9-0.3,4.3c0,15.7,12.7,28.4,28.4,28.4h27.1c38.8,0,77.5,0,116.3,0c0.5,0,1,0,1.5,0l0,0c0.5,0,1,0,1.5,0c38.8,0,77.5,0,116.3,0h27.1c15.7,0,28.4-12.7,28.4-28.4C480.3,438.6,480.2,437.1,480,435.7z"><animate attributeName="stroke-dashoffset" begin="0s" values="1223; 0" dur="3s" repeatCount="1" fill="freeze" calcMode="linear"/><animate attributeName="stroke-dashoffset" begin="svg2.mouseover" values="1223; 0" dur="3s" repeatCount="1" fill="freeze" calcMode="linear"/></path><path  stroke-dasharray="642" stroke-dashoffset="0" stroke="#AABF19" d="M381.2,192.7c0-5,1-23.2,1-27.3c0-40.5-32.9-73.4-73.4-73.4l0,0l0,0h-0.1h-0.1l0,0l0,0c-0.5,0-0.9,0-1.4,0s-0.9,0-1.4,0l0,0l0,0h-0.1h-0.1l0,0l0,0c-40.5,0-73.4,32.9-73.4,73.4c0,4.2,1,22.3,1,27.3c-1.8,0.1-18.2-3.9-16.4,15.8c3.7,41.7,19.7,33.7,20.1,33.7c7.8,25.2,20,41.2,31.9,51.4c18.6,15.9,36.6,17.7,37,17.7c0.5,0,1,0,1.5,0l0,0l0,0c0.5,0,1,0,1.5,0c0.3,0,18.2-1.7,36.7-17.5c12-10.2,24.2-26.3,32.1-51.6c0.5,0,16.4,8.1,20.1-33.7C399.4,188.8,383,192.8,381.2,192.7z"><animate attributeName="stroke-dashoffset" begin="0s" values="642; 0" dur="3s" repeatCount="1" fill="freeze" calcMode="linear"/><animate attributeName="stroke-dashoffset" begin="svg2.mouseover" values="642; 0" dur="3s" repeatCount="1" fill="freeze" calcMode="linear"/></path><path stroke-dasharray="1002" stroke-dashoffset="0" d="M132.3,363.3l0.2-0.6l0.3-0.6c2.2-4.8,5.8-9.5,11.3-14.9c1.2-1.1,4.8-4.7,7.7-6.7l0,0c0.2-0.1,0.4-0.3,0.6-0.4c0.1-0.1,0.2-0.2,0.4-0.2l0,0c2-1.3,4.4-2.4,5.2-2.8c6.5-3.1,20.5-8.8,30.2-12.8c-0.3-0.1-0.6-0.3-1-0.5c-0.3-0.1-0.6-0.3-0.9-0.4c-8.3-3.9-33.9-14.2-33.9-14.2l0,0c-12.8-5.5-26.1-10.3-39.6-15.1l0,0c-2.4,10.1-25.4,76.8-29,86.1l-2.9-62.1c1-1.5,1.6-3.1,2.4-4.7l9.6-18.9c-6.7,5.6-16.8,9.2-28.1,9.2c-11,0-20.9-3.4-27.7-8.8l9.4,18.5c0.8,1.6,1.4,3.3,2.4,4.7L46,380.2c-3.6-9.2-26.6-76-29-86.1l0,0c-13.5,4.8-26.8,9.5-39.6,15.1l0,0c0,0-25.6,10.3-33.9,14.2c-0.3,0.1-0.6,0.3-0.9,0.4c-0.7,0.4-1.2,0.6-1.5,0.8l0,0c-4.3,2.5-7.7,6.3-9.8,10.8l0,0c0,0,0,0,0,0.1c-0.8,1.6-1.4,3.4-1.7,5.2c-3.4,10.4-11,33.6-14.3,46.3c-0.4,1.3-0.8,2.7-1,4.1l0,0l0,0c-0.2,1.2-0.3,2.5-0.3,3.8c0,13.7,11.1,24.7,24.7,24.7h23.6c33.8,0,67.6,0,101.4,0c0.4,0,0.9,0,1.3,0s0.9,0,1.3,0c16.1,0,32.2,0,48.3,0c4.2-14.8,10.9-35.6,15.1-48.1C130.3,368.4,131.5,365.1,132.3,363.3z"><animate attributeName="stroke-dashoffset" begin="0s" values="1002; 0" dur="3s" repeatCount="1" fill="freeze" calcMode="linear"/><animate attributeName="stroke-dashoffset" begin="svg2.mouseover" values="1002; 0" dur="3s" repeatCount="1" fill="freeze" calcMode="linear"/></path><path stroke-dasharray="560" stroke-dashoffset="0" d="M3.4,222.3c6.8,21.9,17.4,35.9,27.8,44.8c16.2,13.9,31.9,15.4,32.2,15.4c0.4,0,0.9,0,1.3,0s0.9,0,1.3,0c0.3,0,15.9-1.5,32-15.3c10.5-8.9,21.1-22.9,28-45c0.4,0,14.3,7.1,17.6-29.3c1.5-17.2-12.8-13.7-14.3-13.8c0-4.4,0.9-20.2,0.9-23.8c0-35.3-28.7-64-64-64l0,0h-0.1H66l0,0l0,0c-0.4,0-0.8,0-1.2,0s-0.8,0-1.2,0l0,0l0,0h-0.1h-0.1l0,0l0,0c-35.3,0-64,28.7-64,64c0,3.6,0.9,19.5,0.9,23.8c-1.6,0.1-15.9-3.4-14.3,13.8C-10.9,229.4,3,222.3,3.4,222.3z"><animate attributeName="stroke-dashoffset" begin="0s" values="560; 0" dur="3s" repeatCount="1" fill="freeze" calcMode="linear"/><animate attributeName="stroke-dashoffset" begin="svg2.mouseover" values="560; 0" dur="3s" repeatCount="1" fill="freeze" calcMode="linear"/></path><path stroke-dasharray="847" stroke-dashoffset="0" d="M700,369.9c-0.2-2.3-0.5-4.6-0.8-6.9c0,0-1.3-7.4-1.6-8.6c-3-13.1-11.1-22.6-22.1-30.1c-2.1-1.4-4.2-2.6-6.3-3.7c-2.8-1.5-45.8-15.6-46.9-16c0.3,1.5,0.4,3.1,0.5,4.9c6,4.2,23.6,19.4,15.6,47.7c0,0-12.7-15.1-25.6-17c-12.3,21.4-33.2,48.6-44.5,55.7c-11.3-7.1-32.2-34.2-44.5-55.7c-12.9,1.9-25.6,17-25.6,17c-8-28.4,9.6-43.5,15.6-47.7c0.1-1.8,0.2-3.4,0.5-4.9c-1.1,0.4-44.1,14.5-46.9,16c-2.2,1.1-4.3,2.3-6.3,3.7c-4.3,2.9-8.1,6.1-11.4,9.7c2.6,1.1,4.8,2.1,6.6,2.9c0.7,0.3,2.8,1.3,4.7,2.5l0,0c4.5,2.7,8.9,6.8,13.7,12.8c4.8,6.1,6.4,9,7.2,10.9c1.2,2.7,2.2,5.6,2.9,8.5c4.1,12.5,10.9,33.3,15,48c22.8,0,45.7,0,68.5,0c33.1,0,66.2,0,99.3,0c9.9,0,18.3-3,25.3-10.4c5.4-5.8,7-12.7,7.4-20.2c0.1-1.3,0.2-7,0.2-7S700,370,700,369.9z"><animate attributeName="stroke-dashoffset" begin="0s" values="847; 0" dur="3s" repeatCount="1" fill="freeze" calcMode="linear"/><animate attributeName="stroke-dashoffset" begin="svg2.mouseover" values="847; 0" dur="3s" repeatCount="1" fill="freeze" calcMode="linear"/></path><path stroke-dasharray="755" stroke-dashoffset="0" d="M490.4,234.9c1,3.7,1.7,7.2,1.7,10.5c0,1.1-0.1,2.2-0.3,3.3c-0.9,3.1-7.5,13.7-8.6,15.5c11.7,12.6,27.1,15.5,42.3,18.7c4.5,0.9,9.1,1.5,14,2.1c1.6,0.3,3.2,0.6,4.8,0.9l-7.7,22.6c0,0-0.2,33,31.7,53.1h0.2c31.9-20.1,31.7-53.1,31.7-53.1l-7.7-22.6c1.6-0.3,3.2-0.6,4.8-0.9c4.9-0.6,9.5-1.2,14-2.1c15.2-3.2,30.7-6.1,42.3-18.7c-1.1-1.8-7.7-12.4-8.6-15.5c-0.2-1.1-0.3-2.2-0.3-3.3c0-3.3,0.6-6.8,1.7-10.5c1-3.5,2.1-7,3.2-10.6c5.2-11.5,8-24.2,8-37.7c0-49.9-39.7-90.5-89-91.4l0,0h-0.1h-0.1l0,0c-49.3,0.9-89,41.5-89,91.4c0,13.4,2.9,26.2,8,37.7 C488.3,227.9,489.4,231.4,490.4,234.9z"><animate attributeName="stroke-dashoffset" begin="0s" values="755; 0" dur="3s" repeatCount="1" fill="freeze" calcMode="linear"/><animate attributeName="stroke-dashoffset" begin="svg2.mouseover" values="755; 0" dur="3s" repeatCount="1" fill="freeze" calcMode="linear"/></path></g></svg>';
        const divSvg3 = document.getElementById('div_svg3');
        divSvg3.innerHTML = '<svg id="svg3" class="svg-image" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="-13 -186.2 932.2 932.2" xml:space="preserve"><g><path stroke-dasharray="2019" stroke-dashoffset="0" d="M73.6,160.8c4.7,16.1,11.2,31.5,19.4,46.1l-23.4,29.5c-7.6,9.7-6.8,23.4,1.8,32.1l40.3,40.3c8.7,8.7,22.5,9.5,32.1,1.8l29.3-23.2c15.1,8.7,31.2,15.5,47.9,20.3l4.4,37.8c1.4,12.2,11.8,21.4,24,21.4h57.1c12.2,0,22.6-9.2,24-21.4l4.2-36.4c18-4.7,35.2-11.7,51.3-20.7l28.4,22.5c9.7,7.6,23.4,6.8,32.1-1.8l40.3-40.3c8.7-8.7,9.5-22.5,1.8-32.1l-22.1-28c9.2-15.9,16.3-32.8,21.1-50.5l34-3.9c12.2-1.4,21.4-11.8,21.4-24V73.1c0-12.2-9.2-22.6-21.4-24l-33.6-3.9c-4.6-17.5-11.5-34.2-20.3-49.9l20.7-26.1c7.6-9.7,6.8-23.4-1.8-32.1l-40.2-40.2c-8.7-8.7-22.5-9.5-32.1-1.8L389.1-85c-16.4-9.7-34-17-52.5-22l-3.8-32.8c-1.4-12.2-11.8-21.4-24-21.4h-57.1c-12.2,0-22.6,9.2-24,21.4l-3.8,32.8c-18.9,5.1-37,12.7-53.8,22.8L143.8-105c-9.7-7.6-23.4-6.8-32.1,1.8L71.4-62.9c-8.7,8.7-9.5,22.5-1.8,32.1l22,27.8C82.7,12.9,76,29.9,71.6,47.5l-35.2,4C24.2,52.9,15,63.2,15,75.5v57.1c0,12.2,9.2,22.6,21.4,24L73.6,160.8z M280.3,6.4c52,0,94.4,42.4,94.4,94.4s-42.4,94.4-94.4,94.4c-52,0-94.4-42.4-94.4-94.4S228.3,6.4,280.3,6.4z"><animate attributeName="stroke-dashoffset" begin="0s" values="2019; 0" dur="3s" repeatCount="1" fill="freeze" calcMode="linear"/><animate attributeName="stroke-dashoffset" begin="svg3.mouseover" values="2019; 0" dur="3s" repeatCount="1" fill="freeze" calcMode="linear"/></path><path stroke-dasharray="1632" stroke-dashoffset="0" d="M844.6,174.9l-30.1-25.4c-9.3-7.8-22.9-7.5-31.7,0.9l-16.6,15.6c-14.1-6.8-29-11.6-44.4-14.3l-4.7-22.9c-2.4-11.9-13.4-20.1-25.4-19.1l-39.3,3.3c-12,1.1-21.5,10.9-21.9,23l-0.8,23.3c-15.1,5.4-29.4,12.9-42.4,22.3l-19.9-13.2c-10.1-6.7-23.6-4.8-31.5,4.5l-25.4,30.3c-7.8,9.3-7.5,22.9,0.9,31.7l17.4,18.5c-6,13.6-10.3,27.8-12.8,42.4l-24.9,5.1c-11.9,2.4-20.1,13.4-19.1,25.4l3.3,39.3c1.1,12,10.9,21.5,23,21.9l26.9,0.9c4.9,12.8,11.3,25,19,36.3l-15,22.7c-6.7,10.1-4.8,23.6,4.5,31.5l30.1,25.4c9.3,7.8,22.9,7.5,31.7-0.9l19.7-18.5c12.9,6,26.5,10.5,40.4,13.2l5.5,27c2.4,11.9,13.4,20.1,25.4,19.1l39.3-3.3c12-1.1,21.5-10.9,21.9-23l0.9-26.4c14.3-5.1,27.9-12,40.4-20.5l21.7,14.3c10.1,6.7,23.6,4.8,31.5-4.5l25.4-30.1c7.8-9.3,7.5-22.9-0.9-31.7l-17.5-18.5c6.4-13.6,11.1-27.9,13.8-42.6l23.9-4.9c11.9-2.4,20.1-13.4,19.1-25.4l-3.3-39.3c-1.1-12-10.9-21.5-23-21.9l-24-0.8c-5-14-11.7-27.1-20-39.4l13.1-19.7C855.8,196.3,853.9,182.7,844.6,174.9z M696.5,402c-42.4,3.6-79.9-28-83.5-70.5c-3.6-42.4,28-79.9,70.5-83.5c42.4-3.6,79.9,28,83.5,70.5C770.6,361,738.9,398.5,696.5,402z"><animate attributeName="stroke-dashoffset" begin="0s" values="1632; 0" dur="3s" repeatCount="1" fill="freeze" calcMode="linear"/><animate attributeName="stroke-dashoffset" begin="svg3.mouseover" values="1632; 0" dur="3s" repeatCount="1" fill="freeze" calcMode="linear"/></path><path stroke-dasharray="1306" stroke-dashoffset="0" stroke="#AABF19" d="M211,507.8c-12,1.2-21.3,11.4-21.4,23.5l-0.3,24.2c-0.2,12.1,8.8,22.5,20.8,24l17.8,2.3c3,10.8,7.2,21.1,12.6,30.9l-11.5,14.1c-7.6,9.5-7.1,23,1.4,31.7l16.9,17.3c8.5,8.7,22.1,9.7,31.7,2.2l14.2-11c10,5.9,20.7,10.6,31.7,13.9l1.9,18.4c1.2,12,11.4,21.3,23.5,21.4l24.2,0.3c12.1,0.2,22.5-8.8,24-20.8l2.2-17.4c12-3,23.5-7.5,34.4-13.4l13.4,10.8c9.5,7.6,23,7.1,31.7-1.4l17.3-16.9c8.7-8.5,9.7-22.1,2.2-31.7l-10.2-13.3c6.3-10.5,11.2-21.7,14.5-33.5l15.9-1.6c12-1.2,21.3-11.4,21.4-23.5l0.3-24.2c0.2-12.1-8.8-22.5-20.8-24l-15.5-2c-3-11.7-7.4-22.9-13.1-33.5l9.7-11.9c7.6-9.5,7.1-23-1.4-31.7l-16.9-17.3c-8.5-8.7-22.1-9.7-31.7-2.2l-11.6,8.9c-10.9-6.6-22.6-11.7-34.8-15.1l-1.5-15c-1.2-12-11.4-21.3-23.5-21.4l-24.2-0.3c-12.1-0.2-22.5,8.8-24,20.8l-1.9,14.9c-12.6,3.2-24.8,8.2-36,14.7l-12-9.8c-9.5-7.6-23-7.1-31.7,1.4l-17.4,17c-8.7,8.5-9.7,22.1-2.2,31.7l10.2,13.2c-5.9,10.5-10.6,21.7-13.7,33.5L211,507.8z M367.1,480.4c34.7,0.4,62.5,29,62.1,63.7c-0.4,34.7-29,62.5-63.7,62.1c-34.7-0.4-62.5-29-62.1-63.7C303.9,507.9,332.4,480.1,367.1,480.4z"><animate attributeName="stroke-dashoffset" begin="0s" values="1306; 0" dur="3s" repeatCount="1" fill="freeze" calcMode="linear"/>.<animate attributeName="stroke-dashoffset" begin="svg3.mouseover" values="1306; 0" dur="3s" repeatCount="1" fill="freeze" calcMode="linear"/></path></g></svg>';

        parallaxContainer.removeEventListener('scroll', isVisibleSVG, false);
    }
};
parallaxContainer.addEventListener('scroll', isVisibleSVG, false);

*/



/*
//active item menu scroll
function sectionPos(eID) {
    let element = [];
    const el = document.getElementById(eID);
    element[0] = elmYPosition(eID);
    element[1] = element[0] + el.offsetHeight;
    return element;
}

const mainScreenPos = sectionPos('main-screen');
const advantPos = sectionPos('advantages');
const teamPos = sectionPos('team');
const projectsPos = sectionPos('projects');
const formPos = sectionPos('form');

function isVisibleSection(eID, arrPos) {
    const container = document.getElementById(eID);
    const screenPos = [];
    screenPos[0] = parallaxContainer.scrollTop;
    screenPos[1] = parallaxContainer.scrollTop + document.documentElement.clientHeight;


    function activeItem() {
        for (let i = 0; i < item.length - 1; i++){
            item[i].classList.remove('active-item');
        }
        const el = document.querySelector(`a[href*="#${eID}"]`);
        el.classList.add('active-item');
    }

    if (arrPos[0] < screenPos[0]) {
        if (arrPos[1] > screenPos[1]) {
            activeItem();
        }
    } 
}

parallaxContainer.onscroll = function() {
    isVisibleSection('main-screen', mainScreenPos);
    isVisibleSection('advantages', advantPos);
    isVisibleSection('team', teamPos);
    isVisibleSection('projects', projectsPos);
    isVisibleSection('form', formPos);
};
*/
