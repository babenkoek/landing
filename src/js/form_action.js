function validate(form) {
	const elems = form.elements;
	console.log(elems);

	function showError(container, message) {
		const messageEmpty = 'Заполните пустое поле.';
		const arr = container.getElementsByTagName('span');
		container.classList.add('is-invalid');
		arr[0].innerHTML = messageEmpty;

		function resetError() {
			arr[0].innerHTML = message;
			container.removeEventListener('click', resetError, false);
		}

		container.addEventListener('click', resetError, false);
	}

	if (!elems.firstName.value || !elems.email.value || !elems.tel.value || !elems.message.value) {

		//resetError(elems.firstName.parentNode, 'Имя должно содержать только буквы.');
		if (!elems.firstName.value) {
	    	showError(elems.firstName.parentNode, 'Имя должно содержать только буквы.');
		}

		//resetError(elems.email.parentNode, 'Некорректный адрес электронной почты.');
		if (!elems.email.value) {
		   	showError(elems.email.parentNode, 'Некорректный адрес электронной почты.');
		}

		//resetError(elems.tel.parentNode, 'Некорректный номер телефона.');
		if (!elems.tel.value) {
		   	showError(elems.tel.parentNode, 'Некорректный номер телефона.');
		}

		//resetError(elems.message.parentNode, '');
		if (!elems.message.value) {
		   	showError(elems.message.parentNode, '');
		}
		
	} else {
		const url = 'http://localhost:8080';
		//правка
		const formData = new FormData(document.getElementById('feedback'));
		console.log(formData);

		const button = document.getElementById('btn-send');
		button.disabled = true;

		function formSuccess() {
			alert('Спасибо, мы ответим Вам, как только рассмотрим обращение.');
			button.disabled = false;
		}

		fetch(url, {
			method: 'POST',
			body: formData
		}).then(function(response) {
			if (respons.status !== 200)
				throw new Error();
			return response.json();
		}).then(function(data) {
			if (data.code === 0)
				formSuccess();
			else 
				throw new Error();
		}).catch ((error) => {
			formError();
		});
	}

	return false;
}