//toogle menu-icon
const li = document.getElementsByTagName('li');
const ul = document.getElementsByTagName('ul');
const minMenu = document.getElementById('menu-toggle');
const header = document.getElementsByTagName('header');
const parallaxContainer = document.getElementById('parallax-main-content');

minMenu.addEventListener('click', function() {
    //turn off parallax 
    parallaxContainer.classList.toggle('showMenu');

    minMenu.classList.toggle('closeMenu');
    ul[0].classList.toggle('showMenu');
    header[0].classList.toggle('showMenu');

    for (let i = li.length - 1; i >= 0; i--) {
        li[i].addEventListener('click', function() {
            ul[0].classList.remove('showMenu');
            minMenu.classList.remove('closeMenu'); 
            header[0].classList.remove('showMenu');   
            //turn on parallax 
            parallaxContainer.classList.remove('showMenu');
        });
    }
});