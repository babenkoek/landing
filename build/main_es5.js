(function () {
  'use strict';

  var main = function (exports) {
    /*! modernizr 3.6.0 (Custom Build) | MIT *
     * https://modernizr.com/download/?-webp-setclasses !*/

    !function (e, n, A) {
      function o(e, n) {
        return typeof e === n;
      }

      function t() {
        var e, n, A, t, a, i, l;

        for (var f in r) if (r.hasOwnProperty(f)) {
          if (e = [], n = r[f], n.name && (e.push(n.name.toLowerCase()), n.options && n.options.aliases && n.options.aliases.length)) for (A = 0; A < n.options.aliases.length; A++) e.push(n.options.aliases[A].toLowerCase());

          for (t = o(n.fn, "function") ? n.fn() : n.fn, a = 0; a < e.length; a++) i = e[a], l = i.split("."), 1 === l.length ? Modernizr[l[0]] = t : (!Modernizr[l[0]] || Modernizr[l[0]] instanceof Boolean || (Modernizr[l[0]] = new Boolean(Modernizr[l[0]])), Modernizr[l[0]][l[1]] = t), s.push((t ? "" : "no-") + l.join("-"));
        }
      }

      function a(e) {
        var n = u.className,
            A = Modernizr._config.classPrefix || "";

        if (c && (n = n.baseVal), Modernizr._config.enableJSClass) {
          var o = new RegExp("(^|\\s)" + A + "no-js(\\s|$)");
          n = n.replace(o, "$1" + A + "js$2");
        }

        Modernizr._config.enableClasses && (n += " " + A + e.join(" " + A), c ? u.className.baseVal = n : u.className = n);
      }

      function i(e, n) {
        if ("object" == typeof e) for (var A in e) f(e, A) && i(A, e[A]);else {
          e = e.toLowerCase();
          var o = e.split("."),
              t = Modernizr[o[0]];
          if (2 == o.length && (t = t[o[1]]), "undefined" != typeof t) return Modernizr;
          n = "function" == typeof n ? n() : n, 1 == o.length ? Modernizr[o[0]] = n : (!Modernizr[o[0]] || Modernizr[o[0]] instanceof Boolean || (Modernizr[o[0]] = new Boolean(Modernizr[o[0]])), Modernizr[o[0]][o[1]] = n), a([(n && 0 != n ? "" : "no-") + o.join("-")]), Modernizr._trigger(e, n);
        }
        return Modernizr;
      }

      var s = [],
          r = [],
          l = {
        _version: "3.6.0",
        _config: {
          classPrefix: "",
          enableClasses: !0,
          enableJSClass: !0,
          usePrefixes: !0
        },
        _q: [],
        on: function (e, n) {
          var A = this;
          setTimeout(function () {
            n(A[e]);
          }, 0);
        },
        addTest: function (e, n, A) {
          r.push({
            name: e,
            fn: n,
            options: A
          });
        },
        addAsyncTest: function (e) {
          r.push({
            name: null,
            fn: e
          });
        }
      },
          Modernizr = function () {};

      Modernizr.prototype = l, Modernizr = new Modernizr();
      var f,
          u = n.documentElement,
          c = "svg" === u.nodeName.toLowerCase();
      !function () {
        var e = {}.hasOwnProperty;
        f = o(e, "undefined") || o(e.call, "undefined") ? function (e, n) {
          return n in e && o(e.constructor.prototype[n], "undefined");
        } : function (n, A) {
          return e.call(n, A);
        };
      }(), l._l = {}, l.on = function (e, n) {
        this._l[e] || (this._l[e] = []), this._l[e].push(n), Modernizr.hasOwnProperty(e) && setTimeout(function () {
          Modernizr._trigger(e, Modernizr[e]);
        }, 0);
      }, l._trigger = function (e, n) {
        if (this._l[e]) {
          var A = this._l[e];
          setTimeout(function () {
            var e, o;

            for (e = 0; e < A.length; e++) (o = A[e])(n);
          }, 0), delete this._l[e];
        }
      }, Modernizr._q.push(function () {
        l.addTest = i;
      }), Modernizr.addAsyncTest(function () {
        function e(e, n, A) {
          function o(n) {
            var o = n && "load" === n.type ? 1 == t.width : !1,
                a = "webp" === e;
            i(e, a && o ? new Boolean(o) : o), A && A(n);
          }

          var t = new Image();
          t.onerror = o, t.onload = o, t.src = n;
        }

        var n = [{
          uri: "data:image/webp;base64,UklGRiQAAABXRUJQVlA4IBgAAAAwAQCdASoBAAEAAwA0JaQAA3AA/vuUAAA=",
          name: "webp"
        }, {
          uri: "data:image/webp;base64,UklGRkoAAABXRUJQVlA4WAoAAAAQAAAAAAAAAAAAQUxQSAwAAAABBxAR/Q9ERP8DAABWUDggGAAAADABAJ0BKgEAAQADADQlpAADcAD++/1QAA==",
          name: "webp.alpha"
        }, {
          uri: "data:image/webp;base64,UklGRlIAAABXRUJQVlA4WAoAAAASAAAAAAAAAAAAQU5JTQYAAAD/////AABBTk1GJgAAAAAAAAAAAAAAAAAAAGQAAABWUDhMDQAAAC8AAAAQBxAREYiI/gcA",
          name: "webp.animation"
        }, {
          uri: "data:image/webp;base64,UklGRh4AAABXRUJQVlA4TBEAAAAvAAAAAAfQ//73v/+BiOh/AAA=",
          name: "webp.lossless"
        }],
            A = n.shift();
        e(A.name, A.uri, function (A) {
          if (A && "load" === A.type) for (var o = 0; o < n.length; o++) e(n[o].name, n[o].uri);
        });
      }), t(), a(s), delete l.addTest, delete l.addAsyncTest;

      for (var p = 0; p < Modernizr._q.length; p++) Modernizr._q[p]();

      e.Modernizr = Modernizr;
    }(window, document);
    /**
     * material-design-lite - Material Design Components in CSS, JS and HTML
     * @version v1.3.0
     * @license Apache-2.0
     * @copyright 2015 Google, Inc.
     * @link https://github.com/google/material-design-lite
     */

    !function () {
      function e(e, t) {
        if (e) {
          if (t.element_.classList.contains(t.CssClasses_.MDL_JS_RIPPLE_EFFECT)) {
            var s = document.createElement("span");
            s.classList.add(t.CssClasses_.MDL_RIPPLE_CONTAINER), s.classList.add(t.CssClasses_.MDL_JS_RIPPLE_EFFECT);
            var i = document.createElement("span");
            i.classList.add(t.CssClasses_.MDL_RIPPLE), s.appendChild(i), e.appendChild(s);
          }

          e.addEventListener("click", function (s) {
            if ("#" === e.getAttribute("href").charAt(0)) {
              s.preventDefault();
              var i = e.href.split("#")[1],
                  n = t.element_.querySelector("#" + i);
              t.resetTabState_(), t.resetPanelState_(), e.classList.add(t.CssClasses_.ACTIVE_CLASS), n.classList.add(t.CssClasses_.ACTIVE_CLASS);
            }
          });
        }
      }

      function t(e, t, s, i) {
        function n() {
          var n = e.href.split("#")[1],
              a = i.content_.querySelector("#" + n);
          i.resetTabState_(t), i.resetPanelState_(s), e.classList.add(i.CssClasses_.IS_ACTIVE), a.classList.add(i.CssClasses_.IS_ACTIVE);
        }

        if (i.tabBar_.classList.contains(i.CssClasses_.JS_RIPPLE_EFFECT)) {
          var a = document.createElement("span");
          a.classList.add(i.CssClasses_.RIPPLE_CONTAINER), a.classList.add(i.CssClasses_.JS_RIPPLE_EFFECT);
          var l = document.createElement("span");
          l.classList.add(i.CssClasses_.RIPPLE), a.appendChild(l), e.appendChild(a);
        }

        i.tabBar_.classList.contains(i.CssClasses_.TAB_MANUAL_SWITCH) || e.addEventListener("click", function (t) {
          "#" === e.getAttribute("href").charAt(0) && (t.preventDefault(), n());
        }), e.show = n;
      }

      var s = {
        upgradeDom: function (e, t) {},
        upgradeElement: function (e, t) {},
        upgradeElements: function (e) {},
        upgradeAllRegistered: function () {},
        registerUpgradedCallback: function (e, t) {},
        register: function (e) {},
        downgradeElements: function (e) {}
      };
      s = function () {
        function e(e, t) {
          for (var s = 0; s < c.length; s++) if (c[s].className === e) return "undefined" != typeof t && (c[s] = t), c[s];

          return !1;
        }

        function t(e) {
          var t = e.getAttribute("data-upgraded");
          return null === t ? [""] : t.split(",");
        }

        function s(e, s) {
          var i = t(e);
          return i.indexOf(s) !== -1;
        }

        function i(e, t, s) {
          if ("CustomEvent" in window && "function" == typeof window.CustomEvent) return new CustomEvent(e, {
            bubbles: t,
            cancelable: s
          });
          var i = document.createEvent("Events");
          return i.initEvent(e, t, s), i;
        }

        function n(t, s) {
          if ("undefined" == typeof t && "undefined" == typeof s) for (var i = 0; i < c.length; i++) n(c[i].className, c[i].cssClass);else {
            var l = t;

            if ("undefined" == typeof s) {
              var o = e(l);
              o && (s = o.cssClass);
            }

            for (var r = document.querySelectorAll("." + s), _ = 0; _ < r.length; _++) a(r[_], l);
          }
        }

        function a(n, a) {
          if (!("object" == typeof n && n instanceof Element)) throw new Error("Invalid argument provided to upgrade MDL element.");
          var l = i("mdl-componentupgrading", !0, !0);

          if (n.dispatchEvent(l), !l.defaultPrevented) {
            var o = t(n),
                r = [];
            if (a) s(n, a) || r.push(e(a));else {
              var _ = n.classList;
              c.forEach(function (e) {
                _.contains(e.cssClass) && r.indexOf(e) === -1 && !s(n, e.className) && r.push(e);
              });
            }

            for (var d, h = 0, u = r.length; h < u; h++) {
              if (d = r[h], !d) throw new Error("Unable to find a registered component for the given class.");
              o.push(d.className), n.setAttribute("data-upgraded", o.join(","));
              var E = new d.classConstructor(n);
              E[C] = d, p.push(E);

              for (var m = 0, L = d.callbacks.length; m < L; m++) d.callbacks[m](n);

              d.widget && (n[d.className] = E);
              var I = i("mdl-componentupgraded", !0, !1);
              n.dispatchEvent(I);
            }
          }
        }

        function l(e) {
          Array.isArray(e) || (e = e instanceof Element ? [e] : Array.prototype.slice.call(e));

          for (var t, s = 0, i = e.length; s < i; s++) t = e[s], t instanceof HTMLElement && (a(t), t.children.length > 0 && l(t.children));
        }

        function o(t) {
          var s = "undefined" == typeof t.widget && "undefined" == typeof t.widget,
              i = !0;
          s || (i = t.widget || t.widget);
          var n = {
            classConstructor: t.constructor || t.constructor,
            className: t.classAsString || t.classAsString,
            cssClass: t.cssClass || t.cssClass,
            widget: i,
            callbacks: []
          };
          if (c.forEach(function (e) {
            if (e.cssClass === n.cssClass) throw new Error("The provided cssClass has already been registered: " + e.cssClass);
            if (e.className === n.className) throw new Error("The provided className has already been registered");
          }), t.constructor.prototype.hasOwnProperty(C)) throw new Error("MDL component classes must not have " + C + " defined as a property.");
          var a = e(t.classAsString, n);
          a || c.push(n);
        }

        function r(t, s) {
          var i = e(t);
          i && i.callbacks.push(s);
        }

        function _() {
          for (var e = 0; e < c.length; e++) n(c[e].className);
        }

        function d(e) {
          if (e) {
            var t = p.indexOf(e);
            p.splice(t, 1);
            var s = e.element_.getAttribute("data-upgraded").split(","),
                n = s.indexOf(e[C].classAsString);
            s.splice(n, 1), e.element_.setAttribute("data-upgraded", s.join(","));
            var a = i("mdl-componentdowngraded", !0, !1);
            e.element_.dispatchEvent(a);
          }
        }

        function h(e) {
          var t = function (e) {
            p.filter(function (t) {
              return t.element_ === e;
            }).forEach(d);
          };

          if (e instanceof Array || e instanceof NodeList) for (var s = 0; s < e.length; s++) t(e[s]);else {
            if (!(e instanceof Node)) throw new Error("Invalid argument provided to downgrade MDL nodes.");
            t(e);
          }
        }

        var c = [],
            p = [],
            C = "mdlComponentConfigInternal_";
        return {
          upgradeDom: n,
          upgradeElement: a,
          upgradeElements: l,
          upgradeAllRegistered: _,
          registerUpgradedCallback: r,
          register: o,
          downgradeElements: h
        };
      }(), s.ComponentConfigPublic, s.ComponentConfig, s.Component, s.upgradeDom = s.upgradeDom, s.upgradeElement = s.upgradeElement, s.upgradeElements = s.upgradeElements, s.upgradeAllRegistered = s.upgradeAllRegistered, s.registerUpgradedCallback = s.registerUpgradedCallback, s.register = s.register, s.downgradeElements = s.downgradeElements, window.componentHandler = s, window.componentHandler = s, window.addEventListener("load", function () {
        "classList" in document.createElement("div") && "querySelector" in document && "addEventListener" in window && Array.prototype.forEach ? (document.documentElement.classList.add("mdl-js"), s.upgradeAllRegistered()) : (s.upgradeElement = function () {}, s.register = function () {});
      }), Date.now || (Date.now = function () {
        return new Date().getTime();
      }, Date.now = Date.now);

      for (var i = ["webkit", "moz"], n = 0; n < i.length && !window.requestAnimationFrame; ++n) {
        var a = i[n];
        window.requestAnimationFrame = window[a + "RequestAnimationFrame"], window.cancelAnimationFrame = window[a + "CancelAnimationFrame"] || window[a + "CancelRequestAnimationFrame"], window.requestAnimationFrame = window.requestAnimationFrame, window.cancelAnimationFrame = window.cancelAnimationFrame;
      }

      if (/iP(ad|hone|od).*OS 6/.test(window.navigator.userAgent) || !window.requestAnimationFrame || !window.cancelAnimationFrame) {
        var l = 0;
        window.requestAnimationFrame = function (e) {
          var t = Date.now(),
              s = Math.max(l + 16, t);
          return setTimeout(function () {
            e(l = s);
          }, s - t);
        }, window.cancelAnimationFrame = clearTimeout, window.requestAnimationFrame = window.requestAnimationFrame, window.cancelAnimationFrame = window.cancelAnimationFrame;
      }

      var o = function (e) {
        this.element_ = e, this.init();
      };

      window.MaterialButton = o, o.prototype.Constant_ = {}, o.prototype.CssClasses_ = {
        RIPPLE_EFFECT: "mdl-js-ripple-effect",
        RIPPLE_CONTAINER: "mdl-button__ripple-container",
        RIPPLE: "mdl-ripple"
      }, o.prototype.blurHandler_ = function (e) {
        e && this.element_.blur();
      }, o.prototype.disable = function () {
        this.element_.disabled = !0;
      }, o.prototype.disable = o.prototype.disable, o.prototype.enable = function () {
        this.element_.disabled = !1;
      }, o.prototype.enable = o.prototype.enable, o.prototype.init = function () {
        if (this.element_) {
          if (this.element_.classList.contains(this.CssClasses_.RIPPLE_EFFECT)) {
            var e = document.createElement("span");
            e.classList.add(this.CssClasses_.RIPPLE_CONTAINER), this.rippleElement_ = document.createElement("span"), this.rippleElement_.classList.add(this.CssClasses_.RIPPLE), e.appendChild(this.rippleElement_), this.boundRippleBlurHandler = this.blurHandler_.bind(this), this.rippleElement_.addEventListener("mouseup", this.boundRippleBlurHandler), this.element_.appendChild(e);
          }

          this.boundButtonBlurHandler = this.blurHandler_.bind(this), this.element_.addEventListener("mouseup", this.boundButtonBlurHandler), this.element_.addEventListener("mouseleave", this.boundButtonBlurHandler);
        }
      }, s.register({
        constructor: o,
        classAsString: "MaterialButton",
        cssClass: "mdl-js-button",
        widget: !0
      });

      var r = function (e) {
        this.element_ = e, this.init();
      };

      window.MaterialCheckbox = r, r.prototype.Constant_ = {
        TINY_TIMEOUT: .001
      }, r.prototype.CssClasses_ = {
        INPUT: "mdl-checkbox__input",
        BOX_OUTLINE: "mdl-checkbox__box-outline",
        FOCUS_HELPER: "mdl-checkbox__focus-helper",
        TICK_OUTLINE: "mdl-checkbox__tick-outline",
        RIPPLE_EFFECT: "mdl-js-ripple-effect",
        RIPPLE_IGNORE_EVENTS: "mdl-js-ripple-effect--ignore-events",
        RIPPLE_CONTAINER: "mdl-checkbox__ripple-container",
        RIPPLE_CENTER: "mdl-ripple--center",
        RIPPLE: "mdl-ripple",
        IS_FOCUSED: "is-focused",
        IS_DISABLED: "is-disabled",
        IS_CHECKED: "is-checked",
        IS_UPGRADED: "is-upgraded"
      }, r.prototype.onChange_ = function (e) {
        this.updateClasses_();
      }, r.prototype.onFocus_ = function (e) {
        this.element_.classList.add(this.CssClasses_.IS_FOCUSED);
      }, r.prototype.onBlur_ = function (e) {
        this.element_.classList.remove(this.CssClasses_.IS_FOCUSED);
      }, r.prototype.onMouseUp_ = function (e) {
        this.blur_();
      }, r.prototype.updateClasses_ = function () {
        this.checkDisabled(), this.checkToggleState();
      }, r.prototype.blur_ = function () {
        window.setTimeout(function () {
          this.inputElement_.blur();
        }.bind(this), this.Constant_.TINY_TIMEOUT);
      }, r.prototype.checkToggleState = function () {
        this.inputElement_.checked ? this.element_.classList.add(this.CssClasses_.IS_CHECKED) : this.element_.classList.remove(this.CssClasses_.IS_CHECKED);
      }, r.prototype.checkToggleState = r.prototype.checkToggleState, r.prototype.checkDisabled = function () {
        this.inputElement_.disabled ? this.element_.classList.add(this.CssClasses_.IS_DISABLED) : this.element_.classList.remove(this.CssClasses_.IS_DISABLED);
      }, r.prototype.checkDisabled = r.prototype.checkDisabled, r.prototype.disable = function () {
        this.inputElement_.disabled = !0, this.updateClasses_();
      }, r.prototype.disable = r.prototype.disable, r.prototype.enable = function () {
        this.inputElement_.disabled = !1, this.updateClasses_();
      }, r.prototype.enable = r.prototype.enable, r.prototype.check = function () {
        this.inputElement_.checked = !0, this.updateClasses_();
      }, r.prototype.check = r.prototype.check, r.prototype.uncheck = function () {
        this.inputElement_.checked = !1, this.updateClasses_();
      }, r.prototype.uncheck = r.prototype.uncheck, r.prototype.init = function () {
        if (this.element_) {
          this.inputElement_ = this.element_.querySelector("." + this.CssClasses_.INPUT);
          var e = document.createElement("span");
          e.classList.add(this.CssClasses_.BOX_OUTLINE);
          var t = document.createElement("span");
          t.classList.add(this.CssClasses_.FOCUS_HELPER);
          var s = document.createElement("span");

          if (s.classList.add(this.CssClasses_.TICK_OUTLINE), e.appendChild(s), this.element_.appendChild(t), this.element_.appendChild(e), this.element_.classList.contains(this.CssClasses_.RIPPLE_EFFECT)) {
            this.element_.classList.add(this.CssClasses_.RIPPLE_IGNORE_EVENTS), this.rippleContainerElement_ = document.createElement("span"), this.rippleContainerElement_.classList.add(this.CssClasses_.RIPPLE_CONTAINER), this.rippleContainerElement_.classList.add(this.CssClasses_.RIPPLE_EFFECT), this.rippleContainerElement_.classList.add(this.CssClasses_.RIPPLE_CENTER), this.boundRippleMouseUp = this.onMouseUp_.bind(this), this.rippleContainerElement_.addEventListener("mouseup", this.boundRippleMouseUp);
            var i = document.createElement("span");
            i.classList.add(this.CssClasses_.RIPPLE), this.rippleContainerElement_.appendChild(i), this.element_.appendChild(this.rippleContainerElement_);
          }

          this.boundInputOnChange = this.onChange_.bind(this), this.boundInputOnFocus = this.onFocus_.bind(this), this.boundInputOnBlur = this.onBlur_.bind(this), this.boundElementMouseUp = this.onMouseUp_.bind(this), this.inputElement_.addEventListener("change", this.boundInputOnChange), this.inputElement_.addEventListener("focus", this.boundInputOnFocus), this.inputElement_.addEventListener("blur", this.boundInputOnBlur), this.element_.addEventListener("mouseup", this.boundElementMouseUp), this.updateClasses_(), this.element_.classList.add(this.CssClasses_.IS_UPGRADED);
        }
      }, s.register({
        constructor: r,
        classAsString: "MaterialCheckbox",
        cssClass: "mdl-js-checkbox",
        widget: !0
      });

      var _ = function (e) {
        this.element_ = e, this.init();
      };

      window.MaterialIconToggle = _, _.prototype.Constant_ = {
        TINY_TIMEOUT: .001
      }, _.prototype.CssClasses_ = {
        INPUT: "mdl-icon-toggle__input",
        JS_RIPPLE_EFFECT: "mdl-js-ripple-effect",
        RIPPLE_IGNORE_EVENTS: "mdl-js-ripple-effect--ignore-events",
        RIPPLE_CONTAINER: "mdl-icon-toggle__ripple-container",
        RIPPLE_CENTER: "mdl-ripple--center",
        RIPPLE: "mdl-ripple",
        IS_FOCUSED: "is-focused",
        IS_DISABLED: "is-disabled",
        IS_CHECKED: "is-checked"
      }, _.prototype.onChange_ = function (e) {
        this.updateClasses_();
      }, _.prototype.onFocus_ = function (e) {
        this.element_.classList.add(this.CssClasses_.IS_FOCUSED);
      }, _.prototype.onBlur_ = function (e) {
        this.element_.classList.remove(this.CssClasses_.IS_FOCUSED);
      }, _.prototype.onMouseUp_ = function (e) {
        this.blur_();
      }, _.prototype.updateClasses_ = function () {
        this.checkDisabled(), this.checkToggleState();
      }, _.prototype.blur_ = function () {
        window.setTimeout(function () {
          this.inputElement_.blur();
        }.bind(this), this.Constant_.TINY_TIMEOUT);
      }, _.prototype.checkToggleState = function () {
        this.inputElement_.checked ? this.element_.classList.add(this.CssClasses_.IS_CHECKED) : this.element_.classList.remove(this.CssClasses_.IS_CHECKED);
      }, _.prototype.checkToggleState = _.prototype.checkToggleState, _.prototype.checkDisabled = function () {
        this.inputElement_.disabled ? this.element_.classList.add(this.CssClasses_.IS_DISABLED) : this.element_.classList.remove(this.CssClasses_.IS_DISABLED);
      }, _.prototype.checkDisabled = _.prototype.checkDisabled, _.prototype.disable = function () {
        this.inputElement_.disabled = !0, this.updateClasses_();
      }, _.prototype.disable = _.prototype.disable, _.prototype.enable = function () {
        this.inputElement_.disabled = !1, this.updateClasses_();
      }, _.prototype.enable = _.prototype.enable, _.prototype.check = function () {
        this.inputElement_.checked = !0, this.updateClasses_();
      }, _.prototype.check = _.prototype.check, _.prototype.uncheck = function () {
        this.inputElement_.checked = !1, this.updateClasses_();
      }, _.prototype.uncheck = _.prototype.uncheck, _.prototype.init = function () {
        if (this.element_) {
          if (this.inputElement_ = this.element_.querySelector("." + this.CssClasses_.INPUT), this.element_.classList.contains(this.CssClasses_.JS_RIPPLE_EFFECT)) {
            this.element_.classList.add(this.CssClasses_.RIPPLE_IGNORE_EVENTS), this.rippleContainerElement_ = document.createElement("span"), this.rippleContainerElement_.classList.add(this.CssClasses_.RIPPLE_CONTAINER), this.rippleContainerElement_.classList.add(this.CssClasses_.JS_RIPPLE_EFFECT), this.rippleContainerElement_.classList.add(this.CssClasses_.RIPPLE_CENTER), this.boundRippleMouseUp = this.onMouseUp_.bind(this), this.rippleContainerElement_.addEventListener("mouseup", this.boundRippleMouseUp);
            var e = document.createElement("span");
            e.classList.add(this.CssClasses_.RIPPLE), this.rippleContainerElement_.appendChild(e), this.element_.appendChild(this.rippleContainerElement_);
          }

          this.boundInputOnChange = this.onChange_.bind(this), this.boundInputOnFocus = this.onFocus_.bind(this), this.boundInputOnBlur = this.onBlur_.bind(this), this.boundElementOnMouseUp = this.onMouseUp_.bind(this), this.inputElement_.addEventListener("change", this.boundInputOnChange), this.inputElement_.addEventListener("focus", this.boundInputOnFocus), this.inputElement_.addEventListener("blur", this.boundInputOnBlur), this.element_.addEventListener("mouseup", this.boundElementOnMouseUp), this.updateClasses_(), this.element_.classList.add("is-upgraded");
        }
      }, s.register({
        constructor: _,
        classAsString: "MaterialIconToggle",
        cssClass: "mdl-js-icon-toggle",
        widget: !0
      });

      var d = function (e) {
        this.element_ = e, this.init();
      };

      window.MaterialMenu = d, d.prototype.Constant_ = {
        TRANSITION_DURATION_SECONDS: .3,
        TRANSITION_DURATION_FRACTION: .8,
        CLOSE_TIMEOUT: 150
      }, d.prototype.Keycodes_ = {
        ENTER: 13,
        ESCAPE: 27,
        SPACE: 32,
        UP_ARROW: 38,
        DOWN_ARROW: 40
      }, d.prototype.CssClasses_ = {
        CONTAINER: "mdl-menu__container",
        OUTLINE: "mdl-menu__outline",
        ITEM: "mdl-menu__item",
        ITEM_RIPPLE_CONTAINER: "mdl-menu__item-ripple-container",
        RIPPLE_EFFECT: "mdl-js-ripple-effect",
        RIPPLE_IGNORE_EVENTS: "mdl-js-ripple-effect--ignore-events",
        RIPPLE: "mdl-ripple",
        IS_UPGRADED: "is-upgraded",
        IS_VISIBLE: "is-visible",
        IS_ANIMATING: "is-animating",
        BOTTOM_LEFT: "mdl-menu--bottom-left",
        BOTTOM_RIGHT: "mdl-menu--bottom-right",
        TOP_LEFT: "mdl-menu--top-left",
        TOP_RIGHT: "mdl-menu--top-right",
        UNALIGNED: "mdl-menu--unaligned"
      }, d.prototype.init = function () {
        if (this.element_) {
          var e = document.createElement("div");
          e.classList.add(this.CssClasses_.CONTAINER), this.element_.parentElement.insertBefore(e, this.element_), this.element_.parentElement.removeChild(this.element_), e.appendChild(this.element_), this.container_ = e;
          var t = document.createElement("div");
          t.classList.add(this.CssClasses_.OUTLINE), this.outline_ = t, e.insertBefore(t, this.element_);
          var s = this.element_.getAttribute("for") || this.element_.getAttribute("data-mdl-for"),
              i = null;
          s && (i = document.getElementById(s), i && (this.forElement_ = i, i.addEventListener("click", this.handleForClick_.bind(this)), i.addEventListener("keydown", this.handleForKeyboardEvent_.bind(this))));
          var n = this.element_.querySelectorAll("." + this.CssClasses_.ITEM);
          this.boundItemKeydown_ = this.handleItemKeyboardEvent_.bind(this), this.boundItemClick_ = this.handleItemClick_.bind(this);

          for (var a = 0; a < n.length; a++) n[a].addEventListener("click", this.boundItemClick_), n[a].tabIndex = "-1", n[a].addEventListener("keydown", this.boundItemKeydown_);

          if (this.element_.classList.contains(this.CssClasses_.RIPPLE_EFFECT)) for (this.element_.classList.add(this.CssClasses_.RIPPLE_IGNORE_EVENTS), a = 0; a < n.length; a++) {
            var l = n[a],
                o = document.createElement("span");
            o.classList.add(this.CssClasses_.ITEM_RIPPLE_CONTAINER);
            var r = document.createElement("span");
            r.classList.add(this.CssClasses_.RIPPLE), o.appendChild(r), l.appendChild(o), l.classList.add(this.CssClasses_.RIPPLE_EFFECT);
          }
          this.element_.classList.contains(this.CssClasses_.BOTTOM_LEFT) && this.outline_.classList.add(this.CssClasses_.BOTTOM_LEFT), this.element_.classList.contains(this.CssClasses_.BOTTOM_RIGHT) && this.outline_.classList.add(this.CssClasses_.BOTTOM_RIGHT), this.element_.classList.contains(this.CssClasses_.TOP_LEFT) && this.outline_.classList.add(this.CssClasses_.TOP_LEFT), this.element_.classList.contains(this.CssClasses_.TOP_RIGHT) && this.outline_.classList.add(this.CssClasses_.TOP_RIGHT), this.element_.classList.contains(this.CssClasses_.UNALIGNED) && this.outline_.classList.add(this.CssClasses_.UNALIGNED), e.classList.add(this.CssClasses_.IS_UPGRADED);
        }
      }, d.prototype.handleForClick_ = function (e) {
        if (this.element_ && this.forElement_) {
          var t = this.forElement_.getBoundingClientRect(),
              s = this.forElement_.parentElement.getBoundingClientRect();
          this.element_.classList.contains(this.CssClasses_.UNALIGNED) || (this.element_.classList.contains(this.CssClasses_.BOTTOM_RIGHT) ? (this.container_.style.right = s.right - t.right + "px", this.container_.style.top = this.forElement_.offsetTop + this.forElement_.offsetHeight + "px") : this.element_.classList.contains(this.CssClasses_.TOP_LEFT) ? (this.container_.style.left = this.forElement_.offsetLeft + "px", this.container_.style.bottom = s.bottom - t.top + "px") : this.element_.classList.contains(this.CssClasses_.TOP_RIGHT) ? (this.container_.style.right = s.right - t.right + "px", this.container_.style.bottom = s.bottom - t.top + "px") : (this.container_.style.left = this.forElement_.offsetLeft + "px", this.container_.style.top = this.forElement_.offsetTop + this.forElement_.offsetHeight + "px"));
        }

        this.toggle(e);
      }, d.prototype.handleForKeyboardEvent_ = function (e) {
        if (this.element_ && this.container_ && this.forElement_) {
          var t = this.element_.querySelectorAll("." + this.CssClasses_.ITEM + ":not([disabled])");
          t && t.length > 0 && this.container_.classList.contains(this.CssClasses_.IS_VISIBLE) && (e.keyCode === this.Keycodes_.UP_ARROW ? (e.preventDefault(), t[t.length - 1].focus()) : e.keyCode === this.Keycodes_.DOWN_ARROW && (e.preventDefault(), t[0].focus()));
        }
      }, d.prototype.handleItemKeyboardEvent_ = function (e) {
        if (this.element_ && this.container_) {
          var t = this.element_.querySelectorAll("." + this.CssClasses_.ITEM + ":not([disabled])");

          if (t && t.length > 0 && this.container_.classList.contains(this.CssClasses_.IS_VISIBLE)) {
            var s = Array.prototype.slice.call(t).indexOf(e.target);
            if (e.keyCode === this.Keycodes_.UP_ARROW) e.preventDefault(), s > 0 ? t[s - 1].focus() : t[t.length - 1].focus();else if (e.keyCode === this.Keycodes_.DOWN_ARROW) e.preventDefault(), t.length > s + 1 ? t[s + 1].focus() : t[0].focus();else if (e.keyCode === this.Keycodes_.SPACE || e.keyCode === this.Keycodes_.ENTER) {
              e.preventDefault();
              var i = new MouseEvent("mousedown");
              e.target.dispatchEvent(i), i = new MouseEvent("mouseup"), e.target.dispatchEvent(i), e.target.click();
            } else e.keyCode === this.Keycodes_.ESCAPE && (e.preventDefault(), this.hide());
          }
        }
      }, d.prototype.handleItemClick_ = function (e) {
        e.target.hasAttribute("disabled") ? e.stopPropagation() : (this.closing_ = !0, window.setTimeout(function (e) {
          this.hide(), this.closing_ = !1;
        }.bind(this), this.Constant_.CLOSE_TIMEOUT));
      }, d.prototype.applyClip_ = function (e, t) {
        this.element_.classList.contains(this.CssClasses_.UNALIGNED) ? this.element_.style.clip = "" : this.element_.classList.contains(this.CssClasses_.BOTTOM_RIGHT) ? this.element_.style.clip = "rect(0 " + t + "px 0 " + t + "px)" : this.element_.classList.contains(this.CssClasses_.TOP_LEFT) ? this.element_.style.clip = "rect(" + e + "px 0 " + e + "px 0)" : this.element_.classList.contains(this.CssClasses_.TOP_RIGHT) ? this.element_.style.clip = "rect(" + e + "px " + t + "px " + e + "px " + t + "px)" : this.element_.style.clip = "";
      }, d.prototype.removeAnimationEndListener_ = function (e) {
        e.target.classList.remove(d.prototype.CssClasses_.IS_ANIMATING);
      }, d.prototype.addAnimationEndListener_ = function () {
        this.element_.addEventListener("transitionend", this.removeAnimationEndListener_), this.element_.addEventListener("webkitTransitionEnd", this.removeAnimationEndListener_);
      }, d.prototype.show = function (e) {
        if (this.element_ && this.container_ && this.outline_) {
          var t = this.element_.getBoundingClientRect().height,
              s = this.element_.getBoundingClientRect().width;
          this.container_.style.width = s + "px", this.container_.style.height = t + "px", this.outline_.style.width = s + "px", this.outline_.style.height = t + "px";

          for (var i = this.Constant_.TRANSITION_DURATION_SECONDS * this.Constant_.TRANSITION_DURATION_FRACTION, n = this.element_.querySelectorAll("." + this.CssClasses_.ITEM), a = 0; a < n.length; a++) {
            var l = null;
            l = this.element_.classList.contains(this.CssClasses_.TOP_LEFT) || this.element_.classList.contains(this.CssClasses_.TOP_RIGHT) ? (t - n[a].offsetTop - n[a].offsetHeight) / t * i + "s" : n[a].offsetTop / t * i + "s", n[a].style.transitionDelay = l;
          }

          this.applyClip_(t, s), window.requestAnimationFrame(function () {
            this.element_.classList.add(this.CssClasses_.IS_ANIMATING), this.element_.style.clip = "rect(0 " + s + "px " + t + "px 0)", this.container_.classList.add(this.CssClasses_.IS_VISIBLE);
          }.bind(this)), this.addAnimationEndListener_();

          var o = function (t) {
            t === e || this.closing_ || t.target.parentNode === this.element_ || (document.removeEventListener("click", o), this.hide());
          }.bind(this);

          document.addEventListener("click", o);
        }
      }, d.prototype.show = d.prototype.show, d.prototype.hide = function () {
        if (this.element_ && this.container_ && this.outline_) {
          for (var e = this.element_.querySelectorAll("." + this.CssClasses_.ITEM), t = 0; t < e.length; t++) e[t].style.removeProperty("transition-delay");

          var s = this.element_.getBoundingClientRect(),
              i = s.height,
              n = s.width;
          this.element_.classList.add(this.CssClasses_.IS_ANIMATING), this.applyClip_(i, n), this.container_.classList.remove(this.CssClasses_.IS_VISIBLE), this.addAnimationEndListener_();
        }
      }, d.prototype.hide = d.prototype.hide, d.prototype.toggle = function (e) {
        this.container_.classList.contains(this.CssClasses_.IS_VISIBLE) ? this.hide() : this.show(e);
      }, d.prototype.toggle = d.prototype.toggle, s.register({
        constructor: d,
        classAsString: "MaterialMenu",
        cssClass: "mdl-js-menu",
        widget: !0
      });

      var h = function (e) {
        this.element_ = e, this.init();
      };

      window.MaterialProgress = h, h.prototype.Constant_ = {}, h.prototype.CssClasses_ = {
        INDETERMINATE_CLASS: "mdl-progress__indeterminate"
      }, h.prototype.setProgress = function (e) {
        this.element_.classList.contains(this.CssClasses_.INDETERMINATE_CLASS) || (this.progressbar_.style.width = e + "%");
      }, h.prototype.setProgress = h.prototype.setProgress, h.prototype.setBuffer = function (e) {
        this.bufferbar_.style.width = e + "%", this.auxbar_.style.width = 100 - e + "%";
      }, h.prototype.setBuffer = h.prototype.setBuffer, h.prototype.init = function () {
        if (this.element_) {
          var e = document.createElement("div");
          e.className = "progressbar bar bar1", this.element_.appendChild(e), this.progressbar_ = e, e = document.createElement("div"), e.className = "bufferbar bar bar2", this.element_.appendChild(e), this.bufferbar_ = e, e = document.createElement("div"), e.className = "auxbar bar bar3", this.element_.appendChild(e), this.auxbar_ = e, this.progressbar_.style.width = "0%", this.bufferbar_.style.width = "100%", this.auxbar_.style.width = "0%", this.element_.classList.add("is-upgraded");
        }
      }, s.register({
        constructor: h,
        classAsString: "MaterialProgress",
        cssClass: "mdl-js-progress",
        widget: !0
      });

      var c = function (e) {
        this.element_ = e, this.init();
      };

      window.MaterialRadio = c, c.prototype.Constant_ = {
        TINY_TIMEOUT: .001
      }, c.prototype.CssClasses_ = {
        IS_FOCUSED: "is-focused",
        IS_DISABLED: "is-disabled",
        IS_CHECKED: "is-checked",
        IS_UPGRADED: "is-upgraded",
        JS_RADIO: "mdl-js-radio",
        RADIO_BTN: "mdl-radio__button",
        RADIO_OUTER_CIRCLE: "mdl-radio__outer-circle",
        RADIO_INNER_CIRCLE: "mdl-radio__inner-circle",
        RIPPLE_EFFECT: "mdl-js-ripple-effect",
        RIPPLE_IGNORE_EVENTS: "mdl-js-ripple-effect--ignore-events",
        RIPPLE_CONTAINER: "mdl-radio__ripple-container",
        RIPPLE_CENTER: "mdl-ripple--center",
        RIPPLE: "mdl-ripple"
      }, c.prototype.onChange_ = function (e) {
        for (var t = document.getElementsByClassName(this.CssClasses_.JS_RADIO), s = 0; s < t.length; s++) {
          var i = t[s].querySelector("." + this.CssClasses_.RADIO_BTN);
          i.getAttribute("name") === this.btnElement_.getAttribute("name") && "undefined" != typeof t[s].MaterialRadio && t[s].MaterialRadio.updateClasses_();
        }
      }, c.prototype.onFocus_ = function (e) {
        this.element_.classList.add(this.CssClasses_.IS_FOCUSED);
      }, c.prototype.onBlur_ = function (e) {
        this.element_.classList.remove(this.CssClasses_.IS_FOCUSED);
      }, c.prototype.onMouseup_ = function (e) {
        this.blur_();
      }, c.prototype.updateClasses_ = function () {
        this.checkDisabled(), this.checkToggleState();
      }, c.prototype.blur_ = function () {
        window.setTimeout(function () {
          this.btnElement_.blur();
        }.bind(this), this.Constant_.TINY_TIMEOUT);
      }, c.prototype.checkDisabled = function () {
        this.btnElement_.disabled ? this.element_.classList.add(this.CssClasses_.IS_DISABLED) : this.element_.classList.remove(this.CssClasses_.IS_DISABLED);
      }, c.prototype.checkDisabled = c.prototype.checkDisabled, c.prototype.checkToggleState = function () {
        this.btnElement_.checked ? this.element_.classList.add(this.CssClasses_.IS_CHECKED) : this.element_.classList.remove(this.CssClasses_.IS_CHECKED);
      }, c.prototype.checkToggleState = c.prototype.checkToggleState, c.prototype.disable = function () {
        this.btnElement_.disabled = !0, this.updateClasses_();
      }, c.prototype.disable = c.prototype.disable, c.prototype.enable = function () {
        this.btnElement_.disabled = !1, this.updateClasses_();
      }, c.prototype.enable = c.prototype.enable, c.prototype.check = function () {
        this.btnElement_.checked = !0, this.onChange_(null);
      }, c.prototype.check = c.prototype.check, c.prototype.uncheck = function () {
        this.btnElement_.checked = !1, this.onChange_(null);
      }, c.prototype.uncheck = c.prototype.uncheck, c.prototype.init = function () {
        if (this.element_) {
          this.btnElement_ = this.element_.querySelector("." + this.CssClasses_.RADIO_BTN), this.boundChangeHandler_ = this.onChange_.bind(this), this.boundFocusHandler_ = this.onChange_.bind(this), this.boundBlurHandler_ = this.onBlur_.bind(this), this.boundMouseUpHandler_ = this.onMouseup_.bind(this);
          var e = document.createElement("span");
          e.classList.add(this.CssClasses_.RADIO_OUTER_CIRCLE);
          var t = document.createElement("span");
          t.classList.add(this.CssClasses_.RADIO_INNER_CIRCLE), this.element_.appendChild(e), this.element_.appendChild(t);
          var s;

          if (this.element_.classList.contains(this.CssClasses_.RIPPLE_EFFECT)) {
            this.element_.classList.add(this.CssClasses_.RIPPLE_IGNORE_EVENTS), s = document.createElement("span"), s.classList.add(this.CssClasses_.RIPPLE_CONTAINER), s.classList.add(this.CssClasses_.RIPPLE_EFFECT), s.classList.add(this.CssClasses_.RIPPLE_CENTER), s.addEventListener("mouseup", this.boundMouseUpHandler_);
            var i = document.createElement("span");
            i.classList.add(this.CssClasses_.RIPPLE), s.appendChild(i), this.element_.appendChild(s);
          }

          this.btnElement_.addEventListener("change", this.boundChangeHandler_), this.btnElement_.addEventListener("focus", this.boundFocusHandler_), this.btnElement_.addEventListener("blur", this.boundBlurHandler_), this.element_.addEventListener("mouseup", this.boundMouseUpHandler_), this.updateClasses_(), this.element_.classList.add(this.CssClasses_.IS_UPGRADED);
        }
      }, s.register({
        constructor: c,
        classAsString: "MaterialRadio",
        cssClass: "mdl-js-radio",
        widget: !0
      });

      var p = function (e) {
        this.element_ = e, this.isIE_ = window.navigator.msPointerEnabled, this.init();
      };

      window.MaterialSlider = p, p.prototype.Constant_ = {}, p.prototype.CssClasses_ = {
        IE_CONTAINER: "mdl-slider__ie-container",
        SLIDER_CONTAINER: "mdl-slider__container",
        BACKGROUND_FLEX: "mdl-slider__background-flex",
        BACKGROUND_LOWER: "mdl-slider__background-lower",
        BACKGROUND_UPPER: "mdl-slider__background-upper",
        IS_LOWEST_VALUE: "is-lowest-value",
        IS_UPGRADED: "is-upgraded"
      }, p.prototype.onInput_ = function (e) {
        this.updateValueStyles_();
      }, p.prototype.onChange_ = function (e) {
        this.updateValueStyles_();
      }, p.prototype.onMouseUp_ = function (e) {
        e.target.blur();
      }, p.prototype.onContainerMouseDown_ = function (e) {
        if (e.target === this.element_.parentElement) {
          e.preventDefault();
          var t = new MouseEvent("mousedown", {
            target: e.target,
            buttons: e.buttons,
            clientX: e.clientX,
            clientY: this.element_.getBoundingClientRect().y
          });
          this.element_.dispatchEvent(t);
        }
      }, p.prototype.updateValueStyles_ = function () {
        var e = (this.element_.value - this.element_.min) / (this.element_.max - this.element_.min);
        0 === e ? this.element_.classList.add(this.CssClasses_.IS_LOWEST_VALUE) : this.element_.classList.remove(this.CssClasses_.IS_LOWEST_VALUE), this.isIE_ || (this.backgroundLower_.style.flex = e, this.backgroundLower_.style.webkitFlex = e, this.backgroundUpper_.style.flex = 1 - e, this.backgroundUpper_.style.webkitFlex = 1 - e);
      }, p.prototype.disable = function () {
        this.element_.disabled = !0;
      }, p.prototype.disable = p.prototype.disable, p.prototype.enable = function () {
        this.element_.disabled = !1;
      }, p.prototype.enable = p.prototype.enable, p.prototype.change = function (e) {
        "undefined" != typeof e && (this.element_.value = e), this.updateValueStyles_();
      }, p.prototype.change = p.prototype.change, p.prototype.init = function () {
        if (this.element_) {
          if (this.isIE_) {
            var e = document.createElement("div");
            e.classList.add(this.CssClasses_.IE_CONTAINER), this.element_.parentElement.insertBefore(e, this.element_), this.element_.parentElement.removeChild(this.element_), e.appendChild(this.element_);
          } else {
            var t = document.createElement("div");
            t.classList.add(this.CssClasses_.SLIDER_CONTAINER), this.element_.parentElement.insertBefore(t, this.element_), this.element_.parentElement.removeChild(this.element_), t.appendChild(this.element_);
            var s = document.createElement("div");
            s.classList.add(this.CssClasses_.BACKGROUND_FLEX), t.appendChild(s), this.backgroundLower_ = document.createElement("div"), this.backgroundLower_.classList.add(this.CssClasses_.BACKGROUND_LOWER), s.appendChild(this.backgroundLower_), this.backgroundUpper_ = document.createElement("div"), this.backgroundUpper_.classList.add(this.CssClasses_.BACKGROUND_UPPER), s.appendChild(this.backgroundUpper_);
          }

          this.boundInputHandler = this.onInput_.bind(this), this.boundChangeHandler = this.onChange_.bind(this), this.boundMouseUpHandler = this.onMouseUp_.bind(this), this.boundContainerMouseDownHandler = this.onContainerMouseDown_.bind(this), this.element_.addEventListener("input", this.boundInputHandler), this.element_.addEventListener("change", this.boundChangeHandler), this.element_.addEventListener("mouseup", this.boundMouseUpHandler), this.element_.parentElement.addEventListener("mousedown", this.boundContainerMouseDownHandler), this.updateValueStyles_(), this.element_.classList.add(this.CssClasses_.IS_UPGRADED);
        }
      }, s.register({
        constructor: p,
        classAsString: "MaterialSlider",
        cssClass: "mdl-js-slider",
        widget: !0
      });

      var C = function (e) {
        if (this.element_ = e, this.textElement_ = this.element_.querySelector("." + this.cssClasses_.MESSAGE), this.actionElement_ = this.element_.querySelector("." + this.cssClasses_.ACTION), !this.textElement_) throw new Error("There must be a message element for a snackbar.");
        if (!this.actionElement_) throw new Error("There must be an action element for a snackbar.");
        this.active = !1, this.actionHandler_ = void 0, this.message_ = void 0, this.actionText_ = void 0, this.queuedNotifications_ = [], this.setActionHidden_(!0);
      };

      window.MaterialSnackbar = C, C.prototype.Constant_ = {
        ANIMATION_LENGTH: 250
      }, C.prototype.cssClasses_ = {
        SNACKBAR: "mdl-snackbar",
        MESSAGE: "mdl-snackbar__text",
        ACTION: "mdl-snackbar__action",
        ACTIVE: "mdl-snackbar--active"
      }, C.prototype.displaySnackbar_ = function () {
        this.element_.setAttribute("aria-hidden", "true"), this.actionHandler_ && (this.actionElement_.textContent = this.actionText_, this.actionElement_.addEventListener("click", this.actionHandler_), this.setActionHidden_(!1)), this.textElement_.textContent = this.message_, this.element_.classList.add(this.cssClasses_.ACTIVE), this.element_.setAttribute("aria-hidden", "false"), setTimeout(this.cleanup_.bind(this), this.timeout_);
      }, C.prototype.showSnackbar = function (e) {
        if (void 0 === e) throw new Error("Please provide a data object with at least a message to display.");
        if (void 0 === e.message) throw new Error("Please provide a message to be displayed.");
        if (e.actionHandler && !e.actionText) throw new Error("Please provide action text with the handler.");
        this.active ? this.queuedNotifications_.push(e) : (this.active = !0, this.message_ = e.message, e.timeout ? this.timeout_ = e.timeout : this.timeout_ = 2750, e.actionHandler && (this.actionHandler_ = e.actionHandler), e.actionText && (this.actionText_ = e.actionText), this.displaySnackbar_());
      }, C.prototype.showSnackbar = C.prototype.showSnackbar, C.prototype.checkQueue_ = function () {
        this.queuedNotifications_.length > 0 && this.showSnackbar(this.queuedNotifications_.shift());
      }, C.prototype.cleanup_ = function () {
        this.element_.classList.remove(this.cssClasses_.ACTIVE), setTimeout(function () {
          this.element_.setAttribute("aria-hidden", "true"), this.textElement_.textContent = "", Boolean(this.actionElement_.getAttribute("aria-hidden")) || (this.setActionHidden_(!0), this.actionElement_.textContent = "", this.actionElement_.removeEventListener("click", this.actionHandler_)), this.actionHandler_ = void 0, this.message_ = void 0, this.actionText_ = void 0, this.active = !1, this.checkQueue_();
        }.bind(this), this.Constant_.ANIMATION_LENGTH);
      }, C.prototype.setActionHidden_ = function (e) {
        e ? this.actionElement_.setAttribute("aria-hidden", "true") : this.actionElement_.removeAttribute("aria-hidden");
      }, s.register({
        constructor: C,
        classAsString: "MaterialSnackbar",
        cssClass: "mdl-js-snackbar",
        widget: !0
      });

      var u = function (e) {
        this.element_ = e, this.init();
      };

      window.MaterialSpinner = u, u.prototype.Constant_ = {
        MDL_SPINNER_LAYER_COUNT: 4
      }, u.prototype.CssClasses_ = {
        MDL_SPINNER_LAYER: "mdl-spinner__layer",
        MDL_SPINNER_CIRCLE_CLIPPER: "mdl-spinner__circle-clipper",
        MDL_SPINNER_CIRCLE: "mdl-spinner__circle",
        MDL_SPINNER_GAP_PATCH: "mdl-spinner__gap-patch",
        MDL_SPINNER_LEFT: "mdl-spinner__left",
        MDL_SPINNER_RIGHT: "mdl-spinner__right"
      }, u.prototype.createLayer = function (e) {
        var t = document.createElement("div");
        t.classList.add(this.CssClasses_.MDL_SPINNER_LAYER), t.classList.add(this.CssClasses_.MDL_SPINNER_LAYER + "-" + e);
        var s = document.createElement("div");
        s.classList.add(this.CssClasses_.MDL_SPINNER_CIRCLE_CLIPPER), s.classList.add(this.CssClasses_.MDL_SPINNER_LEFT);
        var i = document.createElement("div");
        i.classList.add(this.CssClasses_.MDL_SPINNER_GAP_PATCH);
        var n = document.createElement("div");
        n.classList.add(this.CssClasses_.MDL_SPINNER_CIRCLE_CLIPPER), n.classList.add(this.CssClasses_.MDL_SPINNER_RIGHT);

        for (var a = [s, i, n], l = 0; l < a.length; l++) {
          var o = document.createElement("div");
          o.classList.add(this.CssClasses_.MDL_SPINNER_CIRCLE), a[l].appendChild(o);
        }

        t.appendChild(s), t.appendChild(i), t.appendChild(n), this.element_.appendChild(t);
      }, u.prototype.createLayer = u.prototype.createLayer, u.prototype.stop = function () {
        this.element_.classList.remove("is-active");
      }, u.prototype.stop = u.prototype.stop, u.prototype.start = function () {
        this.element_.classList.add("is-active");
      }, u.prototype.start = u.prototype.start, u.prototype.init = function () {
        if (this.element_) {
          for (var e = 1; e <= this.Constant_.MDL_SPINNER_LAYER_COUNT; e++) this.createLayer(e);

          this.element_.classList.add("is-upgraded");
        }
      }, s.register({
        constructor: u,
        classAsString: "MaterialSpinner",
        cssClass: "mdl-js-spinner",
        widget: !0
      });

      var E = function (e) {
        this.element_ = e, this.init();
      };

      window.MaterialSwitch = E, E.prototype.Constant_ = {
        TINY_TIMEOUT: .001
      }, E.prototype.CssClasses_ = {
        INPUT: "mdl-switch__input",
        TRACK: "mdl-switch__track",
        THUMB: "mdl-switch__thumb",
        FOCUS_HELPER: "mdl-switch__focus-helper",
        RIPPLE_EFFECT: "mdl-js-ripple-effect",
        RIPPLE_IGNORE_EVENTS: "mdl-js-ripple-effect--ignore-events",
        RIPPLE_CONTAINER: "mdl-switch__ripple-container",
        RIPPLE_CENTER: "mdl-ripple--center",
        RIPPLE: "mdl-ripple",
        IS_FOCUSED: "is-focused",
        IS_DISABLED: "is-disabled",
        IS_CHECKED: "is-checked"
      }, E.prototype.onChange_ = function (e) {
        this.updateClasses_();
      }, E.prototype.onFocus_ = function (e) {
        this.element_.classList.add(this.CssClasses_.IS_FOCUSED);
      }, E.prototype.onBlur_ = function (e) {
        this.element_.classList.remove(this.CssClasses_.IS_FOCUSED);
      }, E.prototype.onMouseUp_ = function (e) {
        this.blur_();
      }, E.prototype.updateClasses_ = function () {
        this.checkDisabled(), this.checkToggleState();
      }, E.prototype.blur_ = function () {
        window.setTimeout(function () {
          this.inputElement_.blur();
        }.bind(this), this.Constant_.TINY_TIMEOUT);
      }, E.prototype.checkDisabled = function () {
        this.inputElement_.disabled ? this.element_.classList.add(this.CssClasses_.IS_DISABLED) : this.element_.classList.remove(this.CssClasses_.IS_DISABLED);
      }, E.prototype.checkDisabled = E.prototype.checkDisabled, E.prototype.checkToggleState = function () {
        this.inputElement_.checked ? this.element_.classList.add(this.CssClasses_.IS_CHECKED) : this.element_.classList.remove(this.CssClasses_.IS_CHECKED);
      }, E.prototype.checkToggleState = E.prototype.checkToggleState, E.prototype.disable = function () {
        this.inputElement_.disabled = !0, this.updateClasses_();
      }, E.prototype.disable = E.prototype.disable, E.prototype.enable = function () {
        this.inputElement_.disabled = !1, this.updateClasses_();
      }, E.prototype.enable = E.prototype.enable, E.prototype.on = function () {
        this.inputElement_.checked = !0, this.updateClasses_();
      }, E.prototype.on = E.prototype.on, E.prototype.off = function () {
        this.inputElement_.checked = !1, this.updateClasses_();
      }, E.prototype.off = E.prototype.off, E.prototype.init = function () {
        if (this.element_) {
          this.inputElement_ = this.element_.querySelector("." + this.CssClasses_.INPUT);
          var e = document.createElement("div");
          e.classList.add(this.CssClasses_.TRACK);
          var t = document.createElement("div");
          t.classList.add(this.CssClasses_.THUMB);
          var s = document.createElement("span");

          if (s.classList.add(this.CssClasses_.FOCUS_HELPER), t.appendChild(s), this.element_.appendChild(e), this.element_.appendChild(t), this.boundMouseUpHandler = this.onMouseUp_.bind(this), this.element_.classList.contains(this.CssClasses_.RIPPLE_EFFECT)) {
            this.element_.classList.add(this.CssClasses_.RIPPLE_IGNORE_EVENTS), this.rippleContainerElement_ = document.createElement("span"), this.rippleContainerElement_.classList.add(this.CssClasses_.RIPPLE_CONTAINER), this.rippleContainerElement_.classList.add(this.CssClasses_.RIPPLE_EFFECT), this.rippleContainerElement_.classList.add(this.CssClasses_.RIPPLE_CENTER), this.rippleContainerElement_.addEventListener("mouseup", this.boundMouseUpHandler);
            var i = document.createElement("span");
            i.classList.add(this.CssClasses_.RIPPLE), this.rippleContainerElement_.appendChild(i), this.element_.appendChild(this.rippleContainerElement_);
          }

          this.boundChangeHandler = this.onChange_.bind(this), this.boundFocusHandler = this.onFocus_.bind(this), this.boundBlurHandler = this.onBlur_.bind(this), this.inputElement_.addEventListener("change", this.boundChangeHandler), this.inputElement_.addEventListener("focus", this.boundFocusHandler), this.inputElement_.addEventListener("blur", this.boundBlurHandler), this.element_.addEventListener("mouseup", this.boundMouseUpHandler), this.updateClasses_(), this.element_.classList.add("is-upgraded");
        }
      }, s.register({
        constructor: E,
        classAsString: "MaterialSwitch",
        cssClass: "mdl-js-switch",
        widget: !0
      });

      var m = function (e) {
        this.element_ = e, this.init();
      };

      window.MaterialTabs = m, m.prototype.Constant_ = {}, m.prototype.CssClasses_ = {
        TAB_CLASS: "mdl-tabs__tab",
        PANEL_CLASS: "mdl-tabs__panel",
        ACTIVE_CLASS: "is-active",
        UPGRADED_CLASS: "is-upgraded",
        MDL_JS_RIPPLE_EFFECT: "mdl-js-ripple-effect",
        MDL_RIPPLE_CONTAINER: "mdl-tabs__ripple-container",
        MDL_RIPPLE: "mdl-ripple",
        MDL_JS_RIPPLE_EFFECT_IGNORE_EVENTS: "mdl-js-ripple-effect--ignore-events"
      }, m.prototype.initTabs_ = function () {
        this.element_.classList.contains(this.CssClasses_.MDL_JS_RIPPLE_EFFECT) && this.element_.classList.add(this.CssClasses_.MDL_JS_RIPPLE_EFFECT_IGNORE_EVENTS), this.tabs_ = this.element_.querySelectorAll("." + this.CssClasses_.TAB_CLASS), this.panels_ = this.element_.querySelectorAll("." + this.CssClasses_.PANEL_CLASS);

        for (var t = 0; t < this.tabs_.length; t++) new e(this.tabs_[t], this);

        this.element_.classList.add(this.CssClasses_.UPGRADED_CLASS);
      }, m.prototype.resetTabState_ = function () {
        for (var e = 0; e < this.tabs_.length; e++) this.tabs_[e].classList.remove(this.CssClasses_.ACTIVE_CLASS);
      }, m.prototype.resetPanelState_ = function () {
        for (var e = 0; e < this.panels_.length; e++) this.panels_[e].classList.remove(this.CssClasses_.ACTIVE_CLASS);
      }, m.prototype.init = function () {
        this.element_ && this.initTabs_();
      }, s.register({
        constructor: m,
        classAsString: "MaterialTabs",
        cssClass: "mdl-js-tabs"
      });

      var L = function (e) {
        this.element_ = e, this.maxRows = this.Constant_.NO_MAX_ROWS, this.init();
      };

      window.MaterialTextfield = L, L.prototype.Constant_ = {
        NO_MAX_ROWS: -1,
        MAX_ROWS_ATTRIBUTE: "maxrows"
      }, L.prototype.CssClasses_ = {
        LABEL: "mdl-textfield__label",
        INPUT: "mdl-textfield__input",
        IS_DIRTY: "is-dirty",
        IS_FOCUSED: "is-focused",
        IS_DISABLED: "is-disabled",
        IS_INVALID: "is-invalid",
        IS_UPGRADED: "is-upgraded",
        HAS_PLACEHOLDER: "has-placeholder"
      }, L.prototype.onKeyDown_ = function (e) {
        var t = e.target.value.split("\n").length;
        13 === e.keyCode && t >= this.maxRows && e.preventDefault();
      }, L.prototype.onFocus_ = function (e) {
        this.element_.classList.add(this.CssClasses_.IS_FOCUSED);
      }, L.prototype.onBlur_ = function (e) {
        this.element_.classList.remove(this.CssClasses_.IS_FOCUSED);
      }, L.prototype.onReset_ = function (e) {
        this.updateClasses_();
      }, L.prototype.updateClasses_ = function () {
        this.checkDisabled(), this.checkValidity(), this.checkDirty(), this.checkFocus();
      }, L.prototype.checkDisabled = function () {
        this.input_.disabled ? this.element_.classList.add(this.CssClasses_.IS_DISABLED) : this.element_.classList.remove(this.CssClasses_.IS_DISABLED);
      }, L.prototype.checkDisabled = L.prototype.checkDisabled, L.prototype.checkFocus = function () {
        Boolean(this.element_.querySelector(":focus")) ? this.element_.classList.add(this.CssClasses_.IS_FOCUSED) : this.element_.classList.remove(this.CssClasses_.IS_FOCUSED);
      }, L.prototype.checkFocus = L.prototype.checkFocus, L.prototype.checkValidity = function () {
        this.input_.validity && (this.input_.validity.valid ? this.element_.classList.remove(this.CssClasses_.IS_INVALID) : this.element_.classList.add(this.CssClasses_.IS_INVALID));
      }, L.prototype.checkValidity = L.prototype.checkValidity, L.prototype.checkDirty = function () {
        this.input_.value && this.input_.value.length > 0 ? this.element_.classList.add(this.CssClasses_.IS_DIRTY) : this.element_.classList.remove(this.CssClasses_.IS_DIRTY);
      }, L.prototype.checkDirty = L.prototype.checkDirty, L.prototype.disable = function () {
        this.input_.disabled = !0, this.updateClasses_();
      }, L.prototype.disable = L.prototype.disable, L.prototype.enable = function () {
        this.input_.disabled = !1, this.updateClasses_();
      }, L.prototype.enable = L.prototype.enable, L.prototype.change = function (e) {
        this.input_.value = e || "", this.updateClasses_();
      }, L.prototype.change = L.prototype.change, L.prototype.init = function () {
        if (this.element_ && (this.label_ = this.element_.querySelector("." + this.CssClasses_.LABEL), this.input_ = this.element_.querySelector("." + this.CssClasses_.INPUT), this.input_)) {
          this.input_.hasAttribute(this.Constant_.MAX_ROWS_ATTRIBUTE) && (this.maxRows = parseInt(this.input_.getAttribute(this.Constant_.MAX_ROWS_ATTRIBUTE), 10), isNaN(this.maxRows) && (this.maxRows = this.Constant_.NO_MAX_ROWS)), this.input_.hasAttribute("placeholder") && this.element_.classList.add(this.CssClasses_.HAS_PLACEHOLDER), this.boundUpdateClassesHandler = this.updateClasses_.bind(this), this.boundFocusHandler = this.onFocus_.bind(this), this.boundBlurHandler = this.onBlur_.bind(this), this.boundResetHandler = this.onReset_.bind(this), this.input_.addEventListener("input", this.boundUpdateClassesHandler), this.input_.addEventListener("focus", this.boundFocusHandler), this.input_.addEventListener("blur", this.boundBlurHandler), this.input_.addEventListener("reset", this.boundResetHandler), this.maxRows !== this.Constant_.NO_MAX_ROWS && (this.boundKeyDownHandler = this.onKeyDown_.bind(this), this.input_.addEventListener("keydown", this.boundKeyDownHandler));
          var e = this.element_.classList.contains(this.CssClasses_.IS_INVALID);
          this.updateClasses_(), this.element_.classList.add(this.CssClasses_.IS_UPGRADED), e && this.element_.classList.add(this.CssClasses_.IS_INVALID), this.input_.hasAttribute("autofocus") && (this.element_.focus(), this.checkFocus());
        }
      }, s.register({
        constructor: L,
        classAsString: "MaterialTextfield",
        cssClass: "mdl-js-textfield",
        widget: !0
      });

      var I = function (e) {
        this.element_ = e, this.init();
      };

      window.MaterialTooltip = I, I.prototype.Constant_ = {}, I.prototype.CssClasses_ = {
        IS_ACTIVE: "is-active",
        BOTTOM: "mdl-tooltip--bottom",
        LEFT: "mdl-tooltip--left",
        RIGHT: "mdl-tooltip--right",
        TOP: "mdl-tooltip--top"
      }, I.prototype.handleMouseEnter_ = function (e) {
        var t = e.target.getBoundingClientRect(),
            s = t.left + t.width / 2,
            i = t.top + t.height / 2,
            n = -1 * (this.element_.offsetWidth / 2),
            a = -1 * (this.element_.offsetHeight / 2);
        this.element_.classList.contains(this.CssClasses_.LEFT) || this.element_.classList.contains(this.CssClasses_.RIGHT) ? (s = t.width / 2, i + a < 0 ? (this.element_.style.top = "0", this.element_.style.marginTop = "0") : (this.element_.style.top = i + "px", this.element_.style.marginTop = a + "px")) : s + n < 0 ? (this.element_.style.left = "0", this.element_.style.marginLeft = "0") : (this.element_.style.left = s + "px", this.element_.style.marginLeft = n + "px"), this.element_.classList.contains(this.CssClasses_.TOP) ? this.element_.style.top = t.top - this.element_.offsetHeight - 10 + "px" : this.element_.classList.contains(this.CssClasses_.RIGHT) ? this.element_.style.left = t.left + t.width + 10 + "px" : this.element_.classList.contains(this.CssClasses_.LEFT) ? this.element_.style.left = t.left - this.element_.offsetWidth - 10 + "px" : this.element_.style.top = t.top + t.height + 10 + "px", this.element_.classList.add(this.CssClasses_.IS_ACTIVE);
      }, I.prototype.hideTooltip_ = function () {
        this.element_.classList.remove(this.CssClasses_.IS_ACTIVE);
      }, I.prototype.init = function () {
        if (this.element_) {
          var e = this.element_.getAttribute("for") || this.element_.getAttribute("data-mdl-for");
          e && (this.forElement_ = document.getElementById(e)), this.forElement_ && (this.forElement_.hasAttribute("tabindex") || this.forElement_.setAttribute("tabindex", "0"), this.boundMouseEnterHandler = this.handleMouseEnter_.bind(this), this.boundMouseLeaveAndScrollHandler = this.hideTooltip_.bind(this), this.forElement_.addEventListener("mouseenter", this.boundMouseEnterHandler, !1), this.forElement_.addEventListener("touchend", this.boundMouseEnterHandler, !1), this.forElement_.addEventListener("mouseleave", this.boundMouseLeaveAndScrollHandler, !1), window.addEventListener("scroll", this.boundMouseLeaveAndScrollHandler, !0), window.addEventListener("touchstart", this.boundMouseLeaveAndScrollHandler));
        }
      }, s.register({
        constructor: I,
        classAsString: "MaterialTooltip",
        cssClass: "mdl-tooltip"
      });

      var f = function (e) {
        this.element_ = e, this.init();
      };

      window.MaterialLayout = f, f.prototype.Constant_ = {
        MAX_WIDTH: "(max-width: 1024px)",
        TAB_SCROLL_PIXELS: 100,
        RESIZE_TIMEOUT: 100,
        MENU_ICON: "&#xE5D2;",
        CHEVRON_LEFT: "chevron_left",
        CHEVRON_RIGHT: "chevron_right"
      }, f.prototype.Keycodes_ = {
        ENTER: 13,
        ESCAPE: 27,
        SPACE: 32
      }, f.prototype.Mode_ = {
        STANDARD: 0,
        SEAMED: 1,
        WATERFALL: 2,
        SCROLL: 3
      }, f.prototype.CssClasses_ = {
        CONTAINER: "mdl-layout__container",
        HEADER: "mdl-layout__header",
        DRAWER: "mdl-layout__drawer",
        CONTENT: "mdl-layout__content",
        DRAWER_BTN: "mdl-layout__drawer-button",
        ICON: "material-icons",
        JS_RIPPLE_EFFECT: "mdl-js-ripple-effect",
        RIPPLE_CONTAINER: "mdl-layout__tab-ripple-container",
        RIPPLE: "mdl-ripple",
        RIPPLE_IGNORE_EVENTS: "mdl-js-ripple-effect--ignore-events",
        HEADER_SEAMED: "mdl-layout__header--seamed",
        HEADER_WATERFALL: "mdl-layout__header--waterfall",
        HEADER_SCROLL: "mdl-layout__header--scroll",
        FIXED_HEADER: "mdl-layout--fixed-header",
        OBFUSCATOR: "mdl-layout__obfuscator",
        TAB_BAR: "mdl-layout__tab-bar",
        TAB_CONTAINER: "mdl-layout__tab-bar-container",
        TAB: "mdl-layout__tab",
        TAB_BAR_BUTTON: "mdl-layout__tab-bar-button",
        TAB_BAR_LEFT_BUTTON: "mdl-layout__tab-bar-left-button",
        TAB_BAR_RIGHT_BUTTON: "mdl-layout__tab-bar-right-button",
        TAB_MANUAL_SWITCH: "mdl-layout__tab-manual-switch",
        PANEL: "mdl-layout__tab-panel",
        HAS_DRAWER: "has-drawer",
        HAS_TABS: "has-tabs",
        HAS_SCROLLING_HEADER: "has-scrolling-header",
        CASTING_SHADOW: "is-casting-shadow",
        IS_COMPACT: "is-compact",
        IS_SMALL_SCREEN: "is-small-screen",
        IS_DRAWER_OPEN: "is-visible",
        IS_ACTIVE: "is-active",
        IS_UPGRADED: "is-upgraded",
        IS_ANIMATING: "is-animating",
        ON_LARGE_SCREEN: "mdl-layout--large-screen-only",
        ON_SMALL_SCREEN: "mdl-layout--small-screen-only"
      }, f.prototype.contentScrollHandler_ = function () {
        if (!this.header_.classList.contains(this.CssClasses_.IS_ANIMATING)) {
          var e = !this.element_.classList.contains(this.CssClasses_.IS_SMALL_SCREEN) || this.element_.classList.contains(this.CssClasses_.FIXED_HEADER);
          this.content_.scrollTop > 0 && !this.header_.classList.contains(this.CssClasses_.IS_COMPACT) ? (this.header_.classList.add(this.CssClasses_.CASTING_SHADOW), this.header_.classList.add(this.CssClasses_.IS_COMPACT), e && this.header_.classList.add(this.CssClasses_.IS_ANIMATING)) : this.content_.scrollTop <= 0 && this.header_.classList.contains(this.CssClasses_.IS_COMPACT) && (this.header_.classList.remove(this.CssClasses_.CASTING_SHADOW), this.header_.classList.remove(this.CssClasses_.IS_COMPACT), e && this.header_.classList.add(this.CssClasses_.IS_ANIMATING));
        }
      }, f.prototype.keyboardEventHandler_ = function (e) {
        e.keyCode === this.Keycodes_.ESCAPE && this.drawer_.classList.contains(this.CssClasses_.IS_DRAWER_OPEN) && this.toggleDrawer();
      }, f.prototype.screenSizeHandler_ = function () {
        this.screenSizeMediaQuery_.matches ? this.element_.classList.add(this.CssClasses_.IS_SMALL_SCREEN) : (this.element_.classList.remove(this.CssClasses_.IS_SMALL_SCREEN), this.drawer_ && (this.drawer_.classList.remove(this.CssClasses_.IS_DRAWER_OPEN), this.obfuscator_.classList.remove(this.CssClasses_.IS_DRAWER_OPEN)));
      }, f.prototype.drawerToggleHandler_ = function (e) {
        if (e && "keydown" === e.type) {
          if (e.keyCode !== this.Keycodes_.SPACE && e.keyCode !== this.Keycodes_.ENTER) return;
          e.preventDefault();
        }

        this.toggleDrawer();
      }, f.prototype.headerTransitionEndHandler_ = function () {
        this.header_.classList.remove(this.CssClasses_.IS_ANIMATING);
      }, f.prototype.headerClickHandler_ = function () {
        this.header_.classList.contains(this.CssClasses_.IS_COMPACT) && (this.header_.classList.remove(this.CssClasses_.IS_COMPACT), this.header_.classList.add(this.CssClasses_.IS_ANIMATING));
      }, f.prototype.resetTabState_ = function (e) {
        for (var t = 0; t < e.length; t++) e[t].classList.remove(this.CssClasses_.IS_ACTIVE);
      }, f.prototype.resetPanelState_ = function (e) {
        for (var t = 0; t < e.length; t++) e[t].classList.remove(this.CssClasses_.IS_ACTIVE);
      }, f.prototype.toggleDrawer = function () {
        var e = this.element_.querySelector("." + this.CssClasses_.DRAWER_BTN);
        this.drawer_.classList.toggle(this.CssClasses_.IS_DRAWER_OPEN), this.obfuscator_.classList.toggle(this.CssClasses_.IS_DRAWER_OPEN), this.drawer_.classList.contains(this.CssClasses_.IS_DRAWER_OPEN) ? (this.drawer_.setAttribute("aria-hidden", "false"), e.setAttribute("aria-expanded", "true")) : (this.drawer_.setAttribute("aria-hidden", "true"), e.setAttribute("aria-expanded", "false"));
      }, f.prototype.toggleDrawer = f.prototype.toggleDrawer, f.prototype.init = function () {
        if (this.element_) {
          var e = document.createElement("div");
          e.classList.add(this.CssClasses_.CONTAINER);
          var s = this.element_.querySelector(":focus");
          this.element_.parentElement.insertBefore(e, this.element_), this.element_.parentElement.removeChild(this.element_), e.appendChild(this.element_), s && s.focus();

          for (var i = this.element_.childNodes, n = i.length, a = 0; a < n; a++) {
            var l = i[a];
            l.classList && l.classList.contains(this.CssClasses_.HEADER) && (this.header_ = l), l.classList && l.classList.contains(this.CssClasses_.DRAWER) && (this.drawer_ = l), l.classList && l.classList.contains(this.CssClasses_.CONTENT) && (this.content_ = l);
          }

          window.addEventListener("pageshow", function (e) {
            e.persisted && (this.element_.style.overflowY = "hidden", requestAnimationFrame(function () {
              this.element_.style.overflowY = "";
            }.bind(this)));
          }.bind(this), !1), this.header_ && (this.tabBar_ = this.header_.querySelector("." + this.CssClasses_.TAB_BAR));
          var o = this.Mode_.STANDARD;

          if (this.header_ && (this.header_.classList.contains(this.CssClasses_.HEADER_SEAMED) ? o = this.Mode_.SEAMED : this.header_.classList.contains(this.CssClasses_.HEADER_WATERFALL) ? (o = this.Mode_.WATERFALL, this.header_.addEventListener("transitionend", this.headerTransitionEndHandler_.bind(this)), this.header_.addEventListener("click", this.headerClickHandler_.bind(this))) : this.header_.classList.contains(this.CssClasses_.HEADER_SCROLL) && (o = this.Mode_.SCROLL, e.classList.add(this.CssClasses_.HAS_SCROLLING_HEADER)), o === this.Mode_.STANDARD ? (this.header_.classList.add(this.CssClasses_.CASTING_SHADOW), this.tabBar_ && this.tabBar_.classList.add(this.CssClasses_.CASTING_SHADOW)) : o === this.Mode_.SEAMED || o === this.Mode_.SCROLL ? (this.header_.classList.remove(this.CssClasses_.CASTING_SHADOW), this.tabBar_ && this.tabBar_.classList.remove(this.CssClasses_.CASTING_SHADOW)) : o === this.Mode_.WATERFALL && (this.content_.addEventListener("scroll", this.contentScrollHandler_.bind(this)), this.contentScrollHandler_())), this.drawer_) {
            var r = this.element_.querySelector("." + this.CssClasses_.DRAWER_BTN);

            if (!r) {
              r = document.createElement("div"), r.setAttribute("aria-expanded", "false"), r.setAttribute("role", "button"), r.setAttribute("tabindex", "0"), r.classList.add(this.CssClasses_.DRAWER_BTN);

              var _ = document.createElement("i");

              _.classList.add(this.CssClasses_.ICON), _.innerHTML = this.Constant_.MENU_ICON, r.appendChild(_);
            }

            this.drawer_.classList.contains(this.CssClasses_.ON_LARGE_SCREEN) ? r.classList.add(this.CssClasses_.ON_LARGE_SCREEN) : this.drawer_.classList.contains(this.CssClasses_.ON_SMALL_SCREEN) && r.classList.add(this.CssClasses_.ON_SMALL_SCREEN), r.addEventListener("click", this.drawerToggleHandler_.bind(this)), r.addEventListener("keydown", this.drawerToggleHandler_.bind(this)), this.element_.classList.add(this.CssClasses_.HAS_DRAWER), this.element_.classList.contains(this.CssClasses_.FIXED_HEADER) ? this.header_.insertBefore(r, this.header_.firstChild) : this.element_.insertBefore(r, this.content_);
            var d = document.createElement("div");
            d.classList.add(this.CssClasses_.OBFUSCATOR), this.element_.appendChild(d), d.addEventListener("click", this.drawerToggleHandler_.bind(this)), this.obfuscator_ = d, this.drawer_.addEventListener("keydown", this.keyboardEventHandler_.bind(this)), this.drawer_.setAttribute("aria-hidden", "true");
          }

          if (this.screenSizeMediaQuery_ = window.matchMedia(this.Constant_.MAX_WIDTH), this.screenSizeMediaQuery_.addListener(this.screenSizeHandler_.bind(this)), this.screenSizeHandler_(), this.header_ && this.tabBar_) {
            this.element_.classList.add(this.CssClasses_.HAS_TABS);
            var h = document.createElement("div");
            h.classList.add(this.CssClasses_.TAB_CONTAINER), this.header_.insertBefore(h, this.tabBar_), this.header_.removeChild(this.tabBar_);
            var c = document.createElement("div");
            c.classList.add(this.CssClasses_.TAB_BAR_BUTTON), c.classList.add(this.CssClasses_.TAB_BAR_LEFT_BUTTON);
            var p = document.createElement("i");
            p.classList.add(this.CssClasses_.ICON), p.textContent = this.Constant_.CHEVRON_LEFT, c.appendChild(p), c.addEventListener("click", function () {
              this.tabBar_.scrollLeft -= this.Constant_.TAB_SCROLL_PIXELS;
            }.bind(this));
            var C = document.createElement("div");
            C.classList.add(this.CssClasses_.TAB_BAR_BUTTON), C.classList.add(this.CssClasses_.TAB_BAR_RIGHT_BUTTON);
            var u = document.createElement("i");
            u.classList.add(this.CssClasses_.ICON), u.textContent = this.Constant_.CHEVRON_RIGHT, C.appendChild(u), C.addEventListener("click", function () {
              this.tabBar_.scrollLeft += this.Constant_.TAB_SCROLL_PIXELS;
            }.bind(this)), h.appendChild(c), h.appendChild(this.tabBar_), h.appendChild(C);

            var E = function () {
              this.tabBar_.scrollLeft > 0 ? c.classList.add(this.CssClasses_.IS_ACTIVE) : c.classList.remove(this.CssClasses_.IS_ACTIVE), this.tabBar_.scrollLeft < this.tabBar_.scrollWidth - this.tabBar_.offsetWidth ? C.classList.add(this.CssClasses_.IS_ACTIVE) : C.classList.remove(this.CssClasses_.IS_ACTIVE);
            }.bind(this);

            this.tabBar_.addEventListener("scroll", E), E();

            var m = function () {
              this.resizeTimeoutId_ && clearTimeout(this.resizeTimeoutId_), this.resizeTimeoutId_ = setTimeout(function () {
                E(), this.resizeTimeoutId_ = null;
              }.bind(this), this.Constant_.RESIZE_TIMEOUT);
            }.bind(this);

            window.addEventListener("resize", m), this.tabBar_.classList.contains(this.CssClasses_.JS_RIPPLE_EFFECT) && this.tabBar_.classList.add(this.CssClasses_.RIPPLE_IGNORE_EVENTS);

            for (var L = this.tabBar_.querySelectorAll("." + this.CssClasses_.TAB), I = this.content_.querySelectorAll("." + this.CssClasses_.PANEL), f = 0; f < L.length; f++) new t(L[f], L, I, this);
          }

          this.element_.classList.add(this.CssClasses_.IS_UPGRADED);
        }
      }, window.MaterialLayoutTab = t, s.register({
        constructor: f,
        classAsString: "MaterialLayout",
        cssClass: "mdl-js-layout"
      });

      var b = function (e) {
        this.element_ = e, this.init();
      };

      window.MaterialDataTable = b, b.prototype.Constant_ = {}, b.prototype.CssClasses_ = {
        DATA_TABLE: "mdl-data-table",
        SELECTABLE: "mdl-data-table--selectable",
        SELECT_ELEMENT: "mdl-data-table__select",
        IS_SELECTED: "is-selected",
        IS_UPGRADED: "is-upgraded"
      }, b.prototype.selectRow_ = function (e, t, s) {
        return t ? function () {
          e.checked ? t.classList.add(this.CssClasses_.IS_SELECTED) : t.classList.remove(this.CssClasses_.IS_SELECTED);
        }.bind(this) : s ? function () {
          var t, i;
          if (e.checked) for (t = 0; t < s.length; t++) i = s[t].querySelector("td").querySelector(".mdl-checkbox"), i.MaterialCheckbox.check(), s[t].classList.add(this.CssClasses_.IS_SELECTED);else for (t = 0; t < s.length; t++) i = s[t].querySelector("td").querySelector(".mdl-checkbox"), i.MaterialCheckbox.uncheck(), s[t].classList.remove(this.CssClasses_.IS_SELECTED);
        }.bind(this) : void 0;
      }, b.prototype.createCheckbox_ = function (e, t) {
        var i = document.createElement("label"),
            n = ["mdl-checkbox", "mdl-js-checkbox", "mdl-js-ripple-effect", this.CssClasses_.SELECT_ELEMENT];
        i.className = n.join(" ");
        var a = document.createElement("input");
        return a.type = "checkbox", a.classList.add("mdl-checkbox__input"), e ? (a.checked = e.classList.contains(this.CssClasses_.IS_SELECTED), a.addEventListener("change", this.selectRow_(a, e))) : t && a.addEventListener("change", this.selectRow_(a, null, t)), i.appendChild(a), s.upgradeElement(i, "MaterialCheckbox"), i;
      }, b.prototype.init = function () {
        if (this.element_) {
          var e = this.element_.querySelector("th"),
              t = Array.prototype.slice.call(this.element_.querySelectorAll("tbody tr")),
              s = Array.prototype.slice.call(this.element_.querySelectorAll("tfoot tr")),
              i = t.concat(s);

          if (this.element_.classList.contains(this.CssClasses_.SELECTABLE)) {
            var n = document.createElement("th"),
                a = this.createCheckbox_(null, i);
            n.appendChild(a), e.parentElement.insertBefore(n, e);

            for (var l = 0; l < i.length; l++) {
              var o = i[l].querySelector("td");

              if (o) {
                var r = document.createElement("td");

                if ("TBODY" === i[l].parentNode.nodeName.toUpperCase()) {
                  var _ = this.createCheckbox_(i[l]);

                  r.appendChild(_);
                }

                i[l].insertBefore(r, o);
              }
            }

            this.element_.classList.add(this.CssClasses_.IS_UPGRADED);
          }
        }
      }, s.register({
        constructor: b,
        classAsString: "MaterialDataTable",
        cssClass: "mdl-js-data-table"
      });

      var S = function (e) {
        this.element_ = e, this.init();
      };

      window.MaterialRipple = S, S.prototype.Constant_ = {
        INITIAL_SCALE: "scale(0.0001, 0.0001)",
        INITIAL_SIZE: "1px",
        INITIAL_OPACITY: "0.4",
        FINAL_OPACITY: "0",
        FINAL_SCALE: ""
      }, S.prototype.CssClasses_ = {
        RIPPLE_CENTER: "mdl-ripple--center",
        RIPPLE_EFFECT_IGNORE_EVENTS: "mdl-js-ripple-effect--ignore-events",
        RIPPLE: "mdl-ripple",
        IS_ANIMATING: "is-animating",
        IS_VISIBLE: "is-visible"
      }, S.prototype.downHandler_ = function (e) {
        if (!this.rippleElement_.style.width && !this.rippleElement_.style.height) {
          var t = this.element_.getBoundingClientRect();
          this.boundHeight = t.height, this.boundWidth = t.width, this.rippleSize_ = 2 * Math.sqrt(t.width * t.width + t.height * t.height) + 2, this.rippleElement_.style.width = this.rippleSize_ + "px", this.rippleElement_.style.height = this.rippleSize_ + "px";
        }

        if (this.rippleElement_.classList.add(this.CssClasses_.IS_VISIBLE), "mousedown" === e.type && this.ignoringMouseDown_) this.ignoringMouseDown_ = !1;else {
          "touchstart" === e.type && (this.ignoringMouseDown_ = !0);
          var s = this.getFrameCount();
          if (s > 0) return;
          this.setFrameCount(1);
          var i,
              n,
              a = e.currentTarget.getBoundingClientRect();
          if (0 === e.clientX && 0 === e.clientY) i = Math.round(a.width / 2), n = Math.round(a.height / 2);else {
            var l = void 0 !== e.clientX ? e.clientX : e.touches[0].clientX,
                o = void 0 !== e.clientY ? e.clientY : e.touches[0].clientY;
            i = Math.round(l - a.left), n = Math.round(o - a.top);
          }
          this.setRippleXY(i, n), this.setRippleStyles(!0), window.requestAnimationFrame(this.animFrameHandler.bind(this));
        }
      }, S.prototype.upHandler_ = function (e) {
        e && 2 !== e.detail && window.setTimeout(function () {
          this.rippleElement_.classList.remove(this.CssClasses_.IS_VISIBLE);
        }.bind(this), 0);
      }, S.prototype.init = function () {
        if (this.element_) {
          var e = this.element_.classList.contains(this.CssClasses_.RIPPLE_CENTER);
          this.element_.classList.contains(this.CssClasses_.RIPPLE_EFFECT_IGNORE_EVENTS) || (this.rippleElement_ = this.element_.querySelector("." + this.CssClasses_.RIPPLE), this.frameCount_ = 0, this.rippleSize_ = 0, this.x_ = 0, this.y_ = 0, this.ignoringMouseDown_ = !1, this.boundDownHandler = this.downHandler_.bind(this), this.element_.addEventListener("mousedown", this.boundDownHandler), this.element_.addEventListener("touchstart", this.boundDownHandler), this.boundUpHandler = this.upHandler_.bind(this), this.element_.addEventListener("mouseup", this.boundUpHandler), this.element_.addEventListener("mouseleave", this.boundUpHandler), this.element_.addEventListener("touchend", this.boundUpHandler), this.element_.addEventListener("blur", this.boundUpHandler), this.getFrameCount = function () {
            return this.frameCount_;
          }, this.setFrameCount = function (e) {
            this.frameCount_ = e;
          }, this.getRippleElement = function () {
            return this.rippleElement_;
          }, this.setRippleXY = function (e, t) {
            this.x_ = e, this.y_ = t;
          }, this.setRippleStyles = function (t) {
            if (null !== this.rippleElement_) {
              var s,
                  i,
                  n,
                  a = "translate(" + this.x_ + "px, " + this.y_ + "px)";
              t ? (i = this.Constant_.INITIAL_SCALE, n = this.Constant_.INITIAL_SIZE) : (i = this.Constant_.FINAL_SCALE, n = this.rippleSize_ + "px", e && (a = "translate(" + this.boundWidth / 2 + "px, " + this.boundHeight / 2 + "px)")), s = "translate(-50%, -50%) " + a + i, this.rippleElement_.style.webkitTransform = s, this.rippleElement_.style.msTransform = s, this.rippleElement_.style.transform = s, t ? this.rippleElement_.classList.remove(this.CssClasses_.IS_ANIMATING) : this.rippleElement_.classList.add(this.CssClasses_.IS_ANIMATING);
            }
          }, this.animFrameHandler = function () {
            this.frameCount_-- > 0 ? window.requestAnimationFrame(this.animFrameHandler.bind(this)) : this.setRippleStyles(!1);
          });
        }
      }, s.register({
        constructor: S,
        classAsString: "MaterialRipple",
        cssClass: "mdl-js-ripple-effect",
        widget: !1
      });
    }();
    /**
     * Copyright 2016 Google Inc. All Rights Reserved.
     *
     * Licensed under the W3C SOFTWARE AND DOCUMENT NOTICE AND LICENSE.
     *
     *  https://www.w3.org/Consortium/Legal/2015/copyright-software-and-document
     *
     */

    (function (window, document) {
      // Exits early if all IntersectionObserver and IntersectionObserverEntry
      // features are natively supported.
      if ('IntersectionObserver' in window && 'IntersectionObserverEntry' in window && 'intersectionRatio' in window.IntersectionObserverEntry.prototype) {
        // Minimal polyfill for Edge 15's lack of `isIntersecting`
        // See: https://github.com/w3c/IntersectionObserver/issues/211
        if (!('isIntersecting' in window.IntersectionObserverEntry.prototype)) {
          Object.defineProperty(window.IntersectionObserverEntry.prototype, 'isIntersecting', {
            get: function () {
              return this.intersectionRatio > 0;
            }
          });
        }

        return;
      }
      /**
       * Creates the global IntersectionObserverEntry constructor.
       * https://w3c.github.io/IntersectionObserver/#intersection-observer-entry
       * @param {Object} entry A dictionary of instance properties.
       * @constructor
       */


      function IntersectionObserverEntry(entry) {
        this.time = entry.time;
        this.target = entry.target;
        this.rootBounds = entry.rootBounds;
        this.boundingClientRect = entry.boundingClientRect;
        this.intersectionRect = entry.intersectionRect || getEmptyRect();
        this.isIntersecting = !!entry.intersectionRect; // Calculates the intersection ratio.

        var targetRect = this.boundingClientRect;
        var targetArea = targetRect.width * targetRect.height;
        var intersectionRect = this.intersectionRect;
        var intersectionArea = intersectionRect.width * intersectionRect.height; // Sets intersection ratio.

        if (targetArea) {
          this.intersectionRatio = intersectionArea / targetArea;
        } else {
          // If area is zero and is intersecting, sets to 1, otherwise to 0
          this.intersectionRatio = this.isIntersecting ? 1 : 0;
        }
      }
      /**
       * Creates the global IntersectionObserver constructor.
       * https://w3c.github.io/IntersectionObserver/#intersection-observer-interface
       * @param {Function} callback The function to be invoked after intersection
       *     changes have queued. The function is not invoked if the queue has
       *     been emptied by calling the `takeRecords` method.
       * @param {Object=} opt_options Optional configuration options.
       * @constructor
       */


      function IntersectionObserver(callback, opt_options) {
        var options = opt_options || {};

        if (typeof callback != 'function') {
          throw new Error('callback must be a function');
        }

        if (options.root && options.root.nodeType != 1) {
          throw new Error('root must be an Element');
        } // Binds and throttles `this._checkForIntersections`.


        this._checkForIntersections = throttle(this._checkForIntersections.bind(this), this.THROTTLE_TIMEOUT); // Private properties.

        this._callback = callback;
        this._observationTargets = [];
        this._queuedEntries = [];
        this._rootMarginValues = this._parseRootMargin(options.rootMargin); // Public properties.

        this.thresholds = this._initThresholds(options.threshold);
        this.root = options.root || null;
        this.rootMargin = this._rootMarginValues.map(function (margin) {
          return margin.value + margin.unit;
        }).join(' ');
      }
      /**
       * The minimum interval within which the document will be checked for
       * intersection changes.
       */


      IntersectionObserver.prototype.THROTTLE_TIMEOUT = 100;
      /**
       * The frequency in which the polyfill polls for intersection changes.
       * this can be updated on a per instance basis and must be set prior to
       * calling `observe` on the first target.
       */

      IntersectionObserver.prototype.POLL_INTERVAL = null;
      /**
       * Use a mutation observer on the root element
       * to detect intersection changes.
       */

      IntersectionObserver.prototype.USE_MUTATION_OBSERVER = true;
      /**
       * Starts observing a target element for intersection changes based on
       * the thresholds values.
       * @param {Element} target The DOM element to observe.
       */

      IntersectionObserver.prototype.observe = function (target) {
        var isTargetAlreadyObserved = this._observationTargets.some(function (item) {
          return item.element == target;
        });

        if (isTargetAlreadyObserved) {
          return;
        }

        if (!(target && target.nodeType == 1)) {
          throw new Error('target must be an Element');
        }

        this._registerInstance();

        this._observationTargets.push({
          element: target,
          entry: null
        });

        this._monitorIntersections();

        this._checkForIntersections();
      };
      /**
       * Stops observing a target element for intersection changes.
       * @param {Element} target The DOM element to observe.
       */


      IntersectionObserver.prototype.unobserve = function (target) {
        this._observationTargets = this._observationTargets.filter(function (item) {
          return item.element != target;
        });

        if (!this._observationTargets.length) {
          this._unmonitorIntersections();

          this._unregisterInstance();
        }
      };
      /**
       * Stops observing all target elements for intersection changes.
       */


      IntersectionObserver.prototype.disconnect = function () {
        this._observationTargets = [];

        this._unmonitorIntersections();

        this._unregisterInstance();
      };
      /**
       * Returns any queue entries that have not yet been reported to the
       * callback and clears the queue. This can be used in conjunction with the
       * callback to obtain the absolute most up-to-date intersection information.
       * @return {Array} The currently queued entries.
       */


      IntersectionObserver.prototype.takeRecords = function () {
        var records = this._queuedEntries.slice();

        this._queuedEntries = [];
        return records;
      };
      /**
       * Accepts the threshold value from the user configuration object and
       * returns a sorted array of unique threshold values. If a value is not
       * between 0 and 1 and error is thrown.
       * @private
       * @param {Array|number=} opt_threshold An optional threshold value or
       *     a list of threshold values, defaulting to [0].
       * @return {Array} A sorted list of unique and valid threshold values.
       */


      IntersectionObserver.prototype._initThresholds = function (opt_threshold) {
        var threshold = opt_threshold || [0];
        if (!Array.isArray(threshold)) threshold = [threshold];
        return threshold.sort().filter(function (t, i, a) {
          if (typeof t != 'number' || isNaN(t) || t < 0 || t > 1) {
            throw new Error('threshold must be a number between 0 and 1 inclusively');
          }

          return t !== a[i - 1];
        });
      };
      /**
       * Accepts the rootMargin value from the user configuration object
       * and returns an array of the four margin values as an object containing
       * the value and unit properties. If any of the values are not properly
       * formatted or use a unit other than px or %, and error is thrown.
       * @private
       * @param {string=} opt_rootMargin An optional rootMargin value,
       *     defaulting to '0px'.
       * @return {Array<Object>} An array of margin objects with the keys
       *     value and unit.
       */


      IntersectionObserver.prototype._parseRootMargin = function (opt_rootMargin) {
        var marginString = opt_rootMargin || '0px';
        var margins = marginString.split(/\s+/).map(function (margin) {
          var parts = /^(-?\d*\.?\d+)(px|%)$/.exec(margin);

          if (!parts) {
            throw new Error('rootMargin must be specified in pixels or percent');
          }

          return {
            value: parseFloat(parts[1]),
            unit: parts[2]
          };
        }); // Handles shorthand.

        margins[1] = margins[1] || margins[0];
        margins[2] = margins[2] || margins[0];
        margins[3] = margins[3] || margins[1];
        return margins;
      };
      /**
       * Starts polling for intersection changes if the polling is not already
       * happening, and if the page's visibilty state is visible.
       * @private
       */


      IntersectionObserver.prototype._monitorIntersections = function () {
        if (!this._monitoringIntersections) {
          this._monitoringIntersections = true; // If a poll interval is set, use polling instead of listening to
          // resize and scroll events or DOM mutations.

          if (this.POLL_INTERVAL) {
            this._monitoringInterval = setInterval(this._checkForIntersections, this.POLL_INTERVAL);
          } else {
            addEvent(window, 'resize', this._checkForIntersections, true);
            addEvent(document, 'scroll', this._checkForIntersections, true);

            if (this.USE_MUTATION_OBSERVER && 'MutationObserver' in window) {
              this._domObserver = new MutationObserver(this._checkForIntersections);

              this._domObserver.observe(document, {
                attributes: true,
                childList: true,
                characterData: true,
                subtree: true
              });
            }
          }
        }
      };
      /**
       * Stops polling for intersection changes.
       * @private
       */


      IntersectionObserver.prototype._unmonitorIntersections = function () {
        if (this._monitoringIntersections) {
          this._monitoringIntersections = false;
          clearInterval(this._monitoringInterval);
          this._monitoringInterval = null;
          removeEvent(window, 'resize', this._checkForIntersections, true);
          removeEvent(document, 'scroll', this._checkForIntersections, true);

          if (this._domObserver) {
            this._domObserver.disconnect();

            this._domObserver = null;
          }
        }
      };
      /**
       * Scans each observation target for intersection changes and adds them
       * to the internal entries queue. If new entries are found, it
       * schedules the callback to be invoked.
       * @private
       */


      IntersectionObserver.prototype._checkForIntersections = function () {
        var rootIsInDom = this._rootIsInDom();

        var rootRect = rootIsInDom ? this._getRootRect() : getEmptyRect();

        this._observationTargets.forEach(function (item) {
          var target = item.element;
          var targetRect = getBoundingClientRect(target);

          var rootContainsTarget = this._rootContainsTarget(target);

          var oldEntry = item.entry;

          var intersectionRect = rootIsInDom && rootContainsTarget && this._computeTargetAndRootIntersection(target, rootRect);

          var newEntry = item.entry = new IntersectionObserverEntry({
            time: now(),
            target: target,
            boundingClientRect: targetRect,
            rootBounds: rootRect,
            intersectionRect: intersectionRect
          });

          if (!oldEntry) {
            this._queuedEntries.push(newEntry);
          } else if (rootIsInDom && rootContainsTarget) {
            // If the new entry intersection ratio has crossed any of the
            // thresholds, add a new entry.
            if (this._hasCrossedThreshold(oldEntry, newEntry)) {
              this._queuedEntries.push(newEntry);
            }
          } else {
            // If the root is not in the DOM or target is not contained within
            // root but the previous entry for this target had an intersection,
            // add a new record indicating removal.
            if (oldEntry && oldEntry.isIntersecting) {
              this._queuedEntries.push(newEntry);
            }
          }
        }, this);

        if (this._queuedEntries.length) {
          this._callback(this.takeRecords(), this);
        }
      };
      /**
       * Accepts a target and root rect computes the intersection between then
       * following the algorithm in the spec.
       * TODO(philipwalton): at this time clip-path is not considered.
       * https://w3c.github.io/IntersectionObserver/#calculate-intersection-rect-algo
       * @param {Element} target The target DOM element
       * @param {Object} rootRect The bounding rect of the root after being
       *     expanded by the rootMargin value.
       * @return {?Object} The final intersection rect object or undefined if no
       *     intersection is found.
       * @private
       */


      IntersectionObserver.prototype._computeTargetAndRootIntersection = function (target, rootRect) {
        // If the element isn't displayed, an intersection can't happen.
        if (window.getComputedStyle(target).display == 'none') return;
        var targetRect = getBoundingClientRect(target);
        var intersectionRect = targetRect;
        var parent = getParentNode(target);
        var atRoot = false;

        while (!atRoot) {
          var parentRect = null;
          var parentComputedStyle = parent.nodeType == 1 ? window.getComputedStyle(parent) : {}; // If the parent isn't displayed, an intersection can't happen.

          if (parentComputedStyle.display == 'none') return;

          if (parent == this.root || parent == document) {
            atRoot = true;
            parentRect = rootRect;
          } else {
            // If the element has a non-visible overflow, and it's not the <body>
            // or <html> element, update the intersection rect.
            // Note: <body> and <html> cannot be clipped to a rect that's not also
            // the document rect, so no need to compute a new intersection.
            if (parent != document.body && parent != document.documentElement && parentComputedStyle.overflow != 'visible') {
              parentRect = getBoundingClientRect(parent);
            }
          } // If either of the above conditionals set a new parentRect,
          // calculate new intersection data.


          if (parentRect) {
            intersectionRect = computeRectIntersection(parentRect, intersectionRect);
            if (!intersectionRect) break;
          }

          parent = getParentNode(parent);
        }

        return intersectionRect;
      };
      /**
       * Returns the root rect after being expanded by the rootMargin value.
       * @return {Object} The expanded root rect.
       * @private
       */


      IntersectionObserver.prototype._getRootRect = function () {
        var rootRect;

        if (this.root) {
          rootRect = getBoundingClientRect(this.root);
        } else {
          // Use <html>/<body> instead of window since scroll bars affect size.
          var html = document.documentElement;
          var body = document.body;
          rootRect = {
            top: 0,
            left: 0,
            right: html.clientWidth || body.clientWidth,
            width: html.clientWidth || body.clientWidth,
            bottom: html.clientHeight || body.clientHeight,
            height: html.clientHeight || body.clientHeight
          };
        }

        return this._expandRectByRootMargin(rootRect);
      };
      /**
       * Accepts a rect and expands it by the rootMargin value.
       * @param {Object} rect The rect object to expand.
       * @return {Object} The expanded rect.
       * @private
       */


      IntersectionObserver.prototype._expandRectByRootMargin = function (rect) {
        var margins = this._rootMarginValues.map(function (margin, i) {
          return margin.unit == 'px' ? margin.value : margin.value * (i % 2 ? rect.width : rect.height) / 100;
        });

        var newRect = {
          top: rect.top - margins[0],
          right: rect.right + margins[1],
          bottom: rect.bottom + margins[2],
          left: rect.left - margins[3]
        };
        newRect.width = newRect.right - newRect.left;
        newRect.height = newRect.bottom - newRect.top;
        return newRect;
      };
      /**
       * Accepts an old and new entry and returns true if at least one of the
       * threshold values has been crossed.
       * @param {?IntersectionObserverEntry} oldEntry The previous entry for a
       *    particular target element or null if no previous entry exists.
       * @param {IntersectionObserverEntry} newEntry The current entry for a
       *    particular target element.
       * @return {boolean} Returns true if a any threshold has been crossed.
       * @private
       */


      IntersectionObserver.prototype._hasCrossedThreshold = function (oldEntry, newEntry) {
        // To make comparing easier, an entry that has a ratio of 0
        // but does not actually intersect is given a value of -1
        var oldRatio = oldEntry && oldEntry.isIntersecting ? oldEntry.intersectionRatio || 0 : -1;
        var newRatio = newEntry.isIntersecting ? newEntry.intersectionRatio || 0 : -1; // Ignore unchanged ratios

        if (oldRatio === newRatio) return;

        for (var i = 0; i < this.thresholds.length; i++) {
          var threshold = this.thresholds[i]; // Return true if an entry matches a threshold or if the new ratio
          // and the old ratio are on the opposite sides of a threshold.

          if (threshold == oldRatio || threshold == newRatio || threshold < oldRatio !== threshold < newRatio) {
            return true;
          }
        }
      };
      /**
       * Returns whether or not the root element is an element and is in the DOM.
       * @return {boolean} True if the root element is an element and is in the DOM.
       * @private
       */


      IntersectionObserver.prototype._rootIsInDom = function () {
        return !this.root || containsDeep(document, this.root);
      };
      /**
       * Returns whether or not the target element is a child of root.
       * @param {Element} target The target element to check.
       * @return {boolean} True if the target element is a child of root.
       * @private
       */


      IntersectionObserver.prototype._rootContainsTarget = function (target) {
        return containsDeep(this.root || document, target);
      };
      /**
       * Adds the instance to the global IntersectionObserver registry if it isn't
       * already present.
       * @private
       */


      IntersectionObserver.prototype._registerInstance = function () {};
      /**
       * Removes the instance from the global IntersectionObserver registry.
       * @private
       */


      IntersectionObserver.prototype._unregisterInstance = function () {};
      /**
       * Returns the result of the performance.now() method or null in browsers
       * that don't support the API.
       * @return {number} The elapsed time since the page was requested.
       */


      function now() {
        return window.performance && performance.now && performance.now();
      }
      /**
       * Throttles a function and delays its executiong, so it's only called at most
       * once within a given time period.
       * @param {Function} fn The function to throttle.
       * @param {number} timeout The amount of time that must pass before the
       *     function can be called again.
       * @return {Function} The throttled function.
       */


      function throttle(fn, timeout) {
        var timer = null;
        return function () {
          if (!timer) {
            timer = setTimeout(function () {
              fn();
              timer = null;
            }, timeout);
          }
        };
      }
      /**
       * Adds an event handler to a DOM node ensuring cross-browser compatibility.
       * @param {Node} node The DOM node to add the event handler to.
       * @param {string} event The event name.
       * @param {Function} fn The event handler to add.
       * @param {boolean} opt_useCapture Optionally adds the even to the capture
       *     phase. Note: this only works in modern browsers.
       */


      function addEvent(node, event, fn, opt_useCapture) {
        if (typeof node.addEventListener == 'function') {
          node.addEventListener(event, fn, opt_useCapture || false);
        } else if (typeof node.attachEvent == 'function') {
          node.attachEvent('on' + event, fn);
        }
      }
      /**
       * Removes a previously added event handler from a DOM node.
       * @param {Node} node The DOM node to remove the event handler from.
       * @param {string} event The event name.
       * @param {Function} fn The event handler to remove.
       * @param {boolean} opt_useCapture If the event handler was added with this
       *     flag set to true, it should be set to true here in order to remove it.
       */


      function removeEvent(node, event, fn, opt_useCapture) {
        if (typeof node.removeEventListener == 'function') {
          node.removeEventListener(event, fn, opt_useCapture || false);
        } else if (typeof node.detatchEvent == 'function') {
          node.detatchEvent('on' + event, fn);
        }
      }
      /**
       * Returns the intersection between two rect objects.
       * @param {Object} rect1 The first rect.
       * @param {Object} rect2 The second rect.
       * @return {?Object} The intersection rect or undefined if no intersection
       *     is found.
       */


      function computeRectIntersection(rect1, rect2) {
        var top = Math.max(rect1.top, rect2.top);
        var bottom = Math.min(rect1.bottom, rect2.bottom);
        var left = Math.max(rect1.left, rect2.left);
        var right = Math.min(rect1.right, rect2.right);
        var width = right - left;
        var height = bottom - top;
        return width >= 0 && height >= 0 && {
          top: top,
          bottom: bottom,
          left: left,
          right: right,
          width: width,
          height: height
        };
      }
      /**
       * Shims the native getBoundingClientRect for compatibility with older IE.
       * @param {Element} el The element whose bounding rect to get.
       * @return {Object} The (possibly shimmed) rect of the element.
       */


      function getBoundingClientRect(el) {
        var rect;

        try {
          rect = el.getBoundingClientRect();
        } catch (err) {// Ignore Windows 7 IE11 "Unspecified error"
          // https://github.com/w3c/IntersectionObserver/pull/205
        }

        if (!rect) return getEmptyRect(); // Older IE

        if (!(rect.width && rect.height)) {
          rect = {
            top: rect.top,
            right: rect.right,
            bottom: rect.bottom,
            left: rect.left,
            width: rect.right - rect.left,
            height: rect.bottom - rect.top
          };
        }

        return rect;
      }
      /**
       * Returns an empty rect object. An empty rect is returned when an element
       * is not in the DOM.
       * @return {Object} The empty rect.
       */


      function getEmptyRect() {
        return {
          top: 0,
          bottom: 0,
          left: 0,
          right: 0,
          width: 0,
          height: 0
        };
      }
      /**
       * Checks to see if a parent element contains a child elemnt (including inside
       * shadow DOM).
       * @param {Node} parent The parent element.
       * @param {Node} child The child element.
       * @return {boolean} True if the parent node contains the child node.
       */


      function containsDeep(parent, child) {
        var node = child;

        while (node) {
          if (node == parent) return true;
          node = getParentNode(node);
        }

        return false;
      }
      /**
       * Gets the parent node of an element or its host element if the parent node
       * is a shadow root.
       * @param {Node} node The node whose parent to get.
       * @return {Node|null} The parent node or null if no parent exists.
       */


      function getParentNode(node) {
        var parent = node.parentNode;

        if (parent && parent.nodeType == 11 && parent.host) {
          // If the parent is a shadow root, return the host element.
          return parent.host;
        }

        return parent;
      } // Exposes the constructors globally.


      window.IntersectionObserver = IntersectionObserver;
      window.IntersectionObserverEntry = IntersectionObserverEntry;
    })(window, document); //go to top


    const goToTop = () => {
      const button = document.createElement('button');
      const textNode = '<img src="img/arrow_up.svg" alt="вверх" style="width: 24px;">';
      button.innerHTML = textNode;
      button.className = 'mdl-button mdl-js-button mdl-button--icon go-to-top';
      componentHandler.upgradeElement(button);
      document.getElementById('parallax-main-content').appendChild(button);
    }; //scrolling section -> switching menu item


    const optionsSection = {
      root: document.querySelector('#parallax-main-content'),
      rootMargin: '0px',
      threshold: 0.51
    };
    const array = [];
    array.push(document.getElementById('main-screen'));
    array.push(document.getElementById('advantages'));
    array.push(document.getElementById('team'));
    array.push(document.getElementById('projects'));
    array.push(document.getElementById('form'));
    const li = document.getElementsByTagName('li');
    const item = [];

    for (let i = 0; i < li.length; i++) {
      item[i] = li[i].querySelector('a');
    }

    const activeMenu = index => {
      for (let i = li.length - 1; i >= 0; i--) item[i].classList.remove('active-item');

      item[index].classList.add('active-item');
    };

    const handleIntersectionSection = (entries, observerSection) => {
      entries.forEach(entery => {
        if (entery.intersectionRatio > 0) {
          activeMenu(array.indexOf(entery.target) + 1);
          if (entery.target.id === 'advantages') goToTop();
        }
      });
    };

    if ('IntersectionObserver' in window) {
      const observerSection = new IntersectionObserver(handleIntersectionSection, optionsSection);
      array.forEach(section => {
        observerSection.observe(section);
      });
    }

    const images = document.querySelectorAll('.lazy-load');
    const options = {
      root: document.querySelector('#parallax-main-content'),
      rootMargin: '0px',
      threshold: 0.1
    };

    const fetchImage = url => {
      return new Promise((resolve, reject) => {
        const image = new Image();
        image.src = url;
        image.onload = resolve;
        image.onerror = reject;
      });
    };

    const loadImage = image => {
      const src = image.dataset.src;
      fetchImage(src).then(() => {
        image.src = src;
      });
    };

    const handleIntersection = (entries, observer) => {
      entries.forEach(entery => {
        if (entery.intersectionRatio > 0) {
          loadImage(entery.target);
        }
      });
    };

    if ('IntersectionObserver' in window) {
      const observer = new IntersectionObserver(handleIntersection, options);
      images.forEach(img => {
        observer.observe(img);
      });
    } else {
      Array.from(images).forEach(image => loadImage(image));
    } // lazy-load for background


    const divBckgr = document.querySelectorAll('.lazy-load-bckgr');

    const loadImageBckgr = div => {
      let src = div.dataset.url;
      fetchImage(src).then(() => {
        div.style.backgroundImage = `url(${src})`;
      });
    };

    const handleIntersectionBckgr = (entries, observer) => {
      entries.forEach(entery => {
        if (entery.intersectionRatio > 0) {
          loadImageBckgr(entery.target);
          observer.unobserve(entery.target);
        }
      });
    };

    if ('IntersectionObserver' in window) {
      const observerBckgr = new IntersectionObserver(handleIntersectionBckgr, options);
      divBckgr.forEach(div => {
        observerBckgr.observe(div);
      });
    } else {
      Array.from(divBckgr).forEach(div => loadImageBckgr(div));
    } //load image for #main-screen


    Modernizr.on('webp', result => {
      if (result) {
        const block = document.getElementById('main-screen');
        block.classList.add('webp');
      } else {
        const block = document.getElementById('main-screen');
        block.classList.add('no-webp');
      }
    }); //active item menu

    const li$1 = document.getElementsByTagName('li');
    const item$1 = [];

    for (let i = 0; i < li$1.length; i++) {
      item$1[i] = li$1[i].querySelector('a');
    }

    const activeMenu$1 = index => {
      for (let i = li$1.length - 1; i >= 0; i--) item$1[i].classList.remove('active-item');

      item$1[index].classList.add('active-item');
    };

    for (let i = li$1.length - 1; i >= 0; i--) {
      item$1[i].addEventListener('click', function () {
        activeMenu$1(i);
      });
    } //toogle menu-icon


    const li$2 = document.getElementsByTagName('li');
    const ul = document.getElementsByTagName('ul');
    const minMenu = document.getElementById('menu-toggle');
    const header = document.getElementsByTagName('header');
    const parallaxContainer = document.getElementById('parallax-main-content');
    minMenu.addEventListener('click', function () {
      //turn off parallax 
      parallaxContainer.classList.toggle('showMenu');
      minMenu.classList.toggle('closeMenu');
      ul[0].classList.toggle('showMenu');
      header[0].classList.toggle('showMenu');

      for (let i = li$2.length - 1; i >= 0; i--) {
        li$2[i].addEventListener('click', function () {
          ul[0].classList.remove('showMenu');
          minMenu.classList.remove('closeMenu');
          header[0].classList.remove('showMenu'); //turn on parallax 

          parallaxContainer.classList.remove('showMenu');
        });
      }
    }); //animate SVG

    const optionsSvg = {
      root: document.querySelector('#parallax-main-content'),
      rootMargin: '0px',
      threshold: 0.3
    };

    const drawSvg = () => {
      const divSvg1 = document.getElementById('div_svg1');
      divSvg1.innerHTML = '<svg id="svg1" class="svg-image" version="1.1" id="wer" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="-244 46 470 470" xml:space="preserve"><g><path stroke-dasharray="1963" stroke-dashoffset="0" d="M203.9,52.5h-409.1c-9.2,0-16.8,7.5-16.8,16.8v310.1c0,9.2,7.5,16.8,16.8,16.8h154.3v54.1h-66.6c-3.9,0-7.1,3.2-7.1,7.1c0,3.9,3.2,7.1,7.1,7.1h233.8c3.9,0,7.1-3.2,7.1-7.1c0-3.9-3.2-7.1-7.1-7.1H49.6v-54.1h154.3c9.2,0,16.8-7.5,16.8-16.8V69.3C220.7,60.1,213.1,52.5,203.9,52.5z"><animate attributeName="stroke-dashoffset" begin="0s" values="1963; 0" dur="3s" repeatCount="1" fill="freeze" calcMode="linear"/><animate attributeName="stroke-dashoffset" begin="svg1.mouseover" values="1963; 0" dur="3s" repeatCount="1" fill="freeze" calcMode="linear"/></path><path stroke-dasharray="325" stroke-dashoffset="0" stroke="#AABF19" d="M-122.8,301h37.7c3.9,0,7.1-3.2,7.1-7.1V191.5c0-3.9-3.2-7.1-7.1-7.1h-37.7c-3.9,0-7.1,3.2-7.1,7.1v102.5C-129.9,297.9-126.7,301-122.8,301z"><animate attributeName="stroke-dashoffset" begin="0s" values="325; 0" dur="3s" repeatCount="1" fill="freeze" calcMode="linear"/><animate attributeName="stroke-dashoffset" begin="svg1.mouseover" values="325; 0" dur="3s" repeatCount="1" fill="freeze" calcMode="linear"/></path><path stroke-dasharray="392" stroke-dashoffset="0" stroke="#AABF19" d="M-56.9,301h37.7c3.9,0,7.1-3.2,7.1-7.1V158c0-3.9-3.2-7.1-7.1-7.1h-37.7c-3.9,0-7.1,3.2-7.1,7.1v135.9C-64,297.9-60.8,301-56.9,301z"><animate attributeName="stroke-dashoffset" begin="0s" values="392; 0" dur="3s" repeatCount="1" fill="freeze" calcMode="linear"/><animate attributeName="stroke-dashoffset" begin="svg1.mouseover" values="392; 0" dur="3s" repeatCount="1" fill="freeze" calcMode="linear"/></path><path stroke-dasharray="455" stroke-dashoffset="0" stroke="#AABF19" d="M9,301h37.7c3.9,0,7.1-3.2,7.1-7.1V126.5c0-3.9-3.2-7.1-7.1-7.1H9c-3.9,0-7.1,3.2-7.1,7.1v167.5C2,297.9,5.2,301,9,301z"><animate attributeName="stroke-dashoffset" begin="0s" values="455; 0" dur="3s" repeatCount="1" fill="freeze" calcMode="linear"/><animate attributeName="stroke-dashoffset" begin="svg1.mouseover" values="455; 0" dur="3s" repeatCount="1" fill="freeze" calcMode="linear"/></path><path stroke-dasharray="260" stroke-dashoffset="0" stroke="#AABF19" d="M75,301h37.7c3.9,0,7.1-3.2,7.1-7.1v-70c0-3.9-3.2-7.1-7.1-7.1H75c-3.9,0-7.1,3.2-7.1,7.1v70C67.9,297.9,71.1,301,75,301z"><animate attributeName="stroke-dashoffset" begin="0s" values="260; 0" dur="3s" repeatCount="1" fill="freeze" calcMode="linear"/><animate attributeName="stroke-dashoffset" begin="svg1.mouseover" values="260; 0" dur="3s" repeatCount="1" fill="freeze" calcMode="linear"/></path></g><line stroke-dasharray="325" stroke-dashoffset="0" x1="-167.6" y1="343" x2="157.4" y2="343"><animate attributeName="stroke-dashoffset" begin="0s" values="325; 0" dur="3s" repeatCount="1" fill="freeze" calcMode="linear"/><animate attributeName="stroke-dashoffset" begin="svg1.mouseover" values="325; 0" dur="3s" repeatCount="1" fill="freeze" calcMode="linear"/></line></svg>';
      const divSvg2 = document.getElementById('div_svg2');
      divSvg2.innerHTML = '<svg id="svg2" class="svg-image" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"viewBox="-86 -113.5 786.5 786.5" xml:space="preserve"><g><path stroke-dasharray="1223" stroke-dashoffset="0" stroke="#AABF19" d="M480,435.7L480,435.7L480,435.7c-0.2-1.6-0.6-3.2-1.1-4.7c-3.8-14.6-12.5-41.3-16.5-53.2c-0.4-2.1-1.1-4.1-2-6c0,0-6.4-9.6-11.2-12.5c0,0-2.4-1.3-2.7-1.4c-9.5-4.5-38.9-16.3-38.9-16.3l0,0c-14.7-6.4-29.9-11.8-45.4-17.3l0,0l0,0l0,0l0,0c-2.8,11.5-29.1,88.1-33.3,98.8l-3.3-71.2c1.1-1.7,1.9-3.6,2.8-5.4l11-21.7c-7.7,6.4-19.3,10.5-32.3,10.5c-12.7,0-24-3.9-31.7-10.1l10.7,21.2c0.9,1.8,1.7,3.8,2.8,5.4l-3.3,71.2c-4.2-10.6-30.5-87.2-33.3-98.8l0,0l0,0l0,0l0,0c-15.5,5.5-30.7,11-45.4,17.3l0,0c0,0-29.4,11.8-38.9,16.3c-0.3,0.2-2.4,1.2-2.7,1.4c0,0-8.8,7.3-11.2,12.4c0,0-1.6,3.9-2,6c-3.9,11.9-12.6,38.5-16.5,53.2c-0.5,1.5-0.9,3.1-1.1,4.7l0,0l0,0c-0.2,1.4-0.3,2.9-0.3,4.3c0,15.7,12.7,28.4,28.4,28.4h27.1c38.8,0,77.5,0,116.3,0c0.5,0,1,0,1.5,0l0,0c0.5,0,1,0,1.5,0c38.8,0,77.5,0,116.3,0h27.1c15.7,0,28.4-12.7,28.4-28.4C480.3,438.6,480.2,437.1,480,435.7z"><animate attributeName="stroke-dashoffset" begin="0s" values="1223; 0" dur="3s" repeatCount="1" fill="freeze" calcMode="linear"/><animate attributeName="stroke-dashoffset" begin="svg2.mouseover" values="1223; 0" dur="3s" repeatCount="1" fill="freeze" calcMode="linear"/></path><path  stroke-dasharray="642" stroke-dashoffset="0" stroke="#AABF19" d="M381.2,192.7c0-5,1-23.2,1-27.3c0-40.5-32.9-73.4-73.4-73.4l0,0l0,0h-0.1h-0.1l0,0l0,0c-0.5,0-0.9,0-1.4,0s-0.9,0-1.4,0l0,0l0,0h-0.1h-0.1l0,0l0,0c-40.5,0-73.4,32.9-73.4,73.4c0,4.2,1,22.3,1,27.3c-1.8,0.1-18.2-3.9-16.4,15.8c3.7,41.7,19.7,33.7,20.1,33.7c7.8,25.2,20,41.2,31.9,51.4c18.6,15.9,36.6,17.7,37,17.7c0.5,0,1,0,1.5,0l0,0l0,0c0.5,0,1,0,1.5,0c0.3,0,18.2-1.7,36.7-17.5c12-10.2,24.2-26.3,32.1-51.6c0.5,0,16.4,8.1,20.1-33.7C399.4,188.8,383,192.8,381.2,192.7z"><animate attributeName="stroke-dashoffset" begin="0s" values="642; 0" dur="3s" repeatCount="1" fill="freeze" calcMode="linear"/><animate attributeName="stroke-dashoffset" begin="svg2.mouseover" values="642; 0" dur="3s" repeatCount="1" fill="freeze" calcMode="linear"/></path><path stroke-dasharray="1002" stroke-dashoffset="0" d="M132.3,363.3l0.2-0.6l0.3-0.6c2.2-4.8,5.8-9.5,11.3-14.9c1.2-1.1,4.8-4.7,7.7-6.7l0,0c0.2-0.1,0.4-0.3,0.6-0.4c0.1-0.1,0.2-0.2,0.4-0.2l0,0c2-1.3,4.4-2.4,5.2-2.8c6.5-3.1,20.5-8.8,30.2-12.8c-0.3-0.1-0.6-0.3-1-0.5c-0.3-0.1-0.6-0.3-0.9-0.4c-8.3-3.9-33.9-14.2-33.9-14.2l0,0c-12.8-5.5-26.1-10.3-39.6-15.1l0,0c-2.4,10.1-25.4,76.8-29,86.1l-2.9-62.1c1-1.5,1.6-3.1,2.4-4.7l9.6-18.9c-6.7,5.6-16.8,9.2-28.1,9.2c-11,0-20.9-3.4-27.7-8.8l9.4,18.5c0.8,1.6,1.4,3.3,2.4,4.7L46,380.2c-3.6-9.2-26.6-76-29-86.1l0,0c-13.5,4.8-26.8,9.5-39.6,15.1l0,0c0,0-25.6,10.3-33.9,14.2c-0.3,0.1-0.6,0.3-0.9,0.4c-0.7,0.4-1.2,0.6-1.5,0.8l0,0c-4.3,2.5-7.7,6.3-9.8,10.8l0,0c0,0,0,0,0,0.1c-0.8,1.6-1.4,3.4-1.7,5.2c-3.4,10.4-11,33.6-14.3,46.3c-0.4,1.3-0.8,2.7-1,4.1l0,0l0,0c-0.2,1.2-0.3,2.5-0.3,3.8c0,13.7,11.1,24.7,24.7,24.7h23.6c33.8,0,67.6,0,101.4,0c0.4,0,0.9,0,1.3,0s0.9,0,1.3,0c16.1,0,32.2,0,48.3,0c4.2-14.8,10.9-35.6,15.1-48.1C130.3,368.4,131.5,365.1,132.3,363.3z"><animate attributeName="stroke-dashoffset" begin="0s" values="1002; 0" dur="3s" repeatCount="1" fill="freeze" calcMode="linear"/><animate attributeName="stroke-dashoffset" begin="svg2.mouseover" values="1002; 0" dur="3s" repeatCount="1" fill="freeze" calcMode="linear"/></path><path stroke-dasharray="560" stroke-dashoffset="0" d="M3.4,222.3c6.8,21.9,17.4,35.9,27.8,44.8c16.2,13.9,31.9,15.4,32.2,15.4c0.4,0,0.9,0,1.3,0s0.9,0,1.3,0c0.3,0,15.9-1.5,32-15.3c10.5-8.9,21.1-22.9,28-45c0.4,0,14.3,7.1,17.6-29.3c1.5-17.2-12.8-13.7-14.3-13.8c0-4.4,0.9-20.2,0.9-23.8c0-35.3-28.7-64-64-64l0,0h-0.1H66l0,0l0,0c-0.4,0-0.8,0-1.2,0s-0.8,0-1.2,0l0,0l0,0h-0.1h-0.1l0,0l0,0c-35.3,0-64,28.7-64,64c0,3.6,0.9,19.5,0.9,23.8c-1.6,0.1-15.9-3.4-14.3,13.8C-10.9,229.4,3,222.3,3.4,222.3z"><animate attributeName="stroke-dashoffset" begin="0s" values="560; 0" dur="3s" repeatCount="1" fill="freeze" calcMode="linear"/><animate attributeName="stroke-dashoffset" begin="svg2.mouseover" values="560; 0" dur="3s" repeatCount="1" fill="freeze" calcMode="linear"/></path><path stroke-dasharray="847" stroke-dashoffset="0" d="M700,369.9c-0.2-2.3-0.5-4.6-0.8-6.9c0,0-1.3-7.4-1.6-8.6c-3-13.1-11.1-22.6-22.1-30.1c-2.1-1.4-4.2-2.6-6.3-3.7c-2.8-1.5-45.8-15.6-46.9-16c0.3,1.5,0.4,3.1,0.5,4.9c6,4.2,23.6,19.4,15.6,47.7c0,0-12.7-15.1-25.6-17c-12.3,21.4-33.2,48.6-44.5,55.7c-11.3-7.1-32.2-34.2-44.5-55.7c-12.9,1.9-25.6,17-25.6,17c-8-28.4,9.6-43.5,15.6-47.7c0.1-1.8,0.2-3.4,0.5-4.9c-1.1,0.4-44.1,14.5-46.9,16c-2.2,1.1-4.3,2.3-6.3,3.7c-4.3,2.9-8.1,6.1-11.4,9.7c2.6,1.1,4.8,2.1,6.6,2.9c0.7,0.3,2.8,1.3,4.7,2.5l0,0c4.5,2.7,8.9,6.8,13.7,12.8c4.8,6.1,6.4,9,7.2,10.9c1.2,2.7,2.2,5.6,2.9,8.5c4.1,12.5,10.9,33.3,15,48c22.8,0,45.7,0,68.5,0c33.1,0,66.2,0,99.3,0c9.9,0,18.3-3,25.3-10.4c5.4-5.8,7-12.7,7.4-20.2c0.1-1.3,0.2-7,0.2-7S700,370,700,369.9z"><animate attributeName="stroke-dashoffset" begin="0s" values="847; 0" dur="3s" repeatCount="1" fill="freeze" calcMode="linear"/><animate attributeName="stroke-dashoffset" begin="svg2.mouseover" values="847; 0" dur="3s" repeatCount="1" fill="freeze" calcMode="linear"/></path><path stroke-dasharray="755" stroke-dashoffset="0" d="M490.4,234.9c1,3.7,1.7,7.2,1.7,10.5c0,1.1-0.1,2.2-0.3,3.3c-0.9,3.1-7.5,13.7-8.6,15.5c11.7,12.6,27.1,15.5,42.3,18.7c4.5,0.9,9.1,1.5,14,2.1c1.6,0.3,3.2,0.6,4.8,0.9l-7.7,22.6c0,0-0.2,33,31.7,53.1h0.2c31.9-20.1,31.7-53.1,31.7-53.1l-7.7-22.6c1.6-0.3,3.2-0.6,4.8-0.9c4.9-0.6,9.5-1.2,14-2.1c15.2-3.2,30.7-6.1,42.3-18.7c-1.1-1.8-7.7-12.4-8.6-15.5c-0.2-1.1-0.3-2.2-0.3-3.3c0-3.3,0.6-6.8,1.7-10.5c1-3.5,2.1-7,3.2-10.6c5.2-11.5,8-24.2,8-37.7c0-49.9-39.7-90.5-89-91.4l0,0h-0.1h-0.1l0,0c-49.3,0.9-89,41.5-89,91.4c0,13.4,2.9,26.2,8,37.7 C488.3,227.9,489.4,231.4,490.4,234.9z"><animate attributeName="stroke-dashoffset" begin="0s" values="755; 0" dur="3s" repeatCount="1" fill="freeze" calcMode="linear"/><animate attributeName="stroke-dashoffset" begin="svg2.mouseover" values="755; 0" dur="3s" repeatCount="1" fill="freeze" calcMode="linear"/></path></g></svg>';
      const divSvg3 = document.getElementById('div_svg3');
      divSvg3.innerHTML = '<svg id="svg3" class="svg-image" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="-13 -186.2 932.2 932.2" xml:space="preserve"><g><path stroke-dasharray="2019" stroke-dashoffset="0" d="M73.6,160.8c4.7,16.1,11.2,31.5,19.4,46.1l-23.4,29.5c-7.6,9.7-6.8,23.4,1.8,32.1l40.3,40.3c8.7,8.7,22.5,9.5,32.1,1.8l29.3-23.2c15.1,8.7,31.2,15.5,47.9,20.3l4.4,37.8c1.4,12.2,11.8,21.4,24,21.4h57.1c12.2,0,22.6-9.2,24-21.4l4.2-36.4c18-4.7,35.2-11.7,51.3-20.7l28.4,22.5c9.7,7.6,23.4,6.8,32.1-1.8l40.3-40.3c8.7-8.7,9.5-22.5,1.8-32.1l-22.1-28c9.2-15.9,16.3-32.8,21.1-50.5l34-3.9c12.2-1.4,21.4-11.8,21.4-24V73.1c0-12.2-9.2-22.6-21.4-24l-33.6-3.9c-4.6-17.5-11.5-34.2-20.3-49.9l20.7-26.1c7.6-9.7,6.8-23.4-1.8-32.1l-40.2-40.2c-8.7-8.7-22.5-9.5-32.1-1.8L389.1-85c-16.4-9.7-34-17-52.5-22l-3.8-32.8c-1.4-12.2-11.8-21.4-24-21.4h-57.1c-12.2,0-22.6,9.2-24,21.4l-3.8,32.8c-18.9,5.1-37,12.7-53.8,22.8L143.8-105c-9.7-7.6-23.4-6.8-32.1,1.8L71.4-62.9c-8.7,8.7-9.5,22.5-1.8,32.1l22,27.8C82.7,12.9,76,29.9,71.6,47.5l-35.2,4C24.2,52.9,15,63.2,15,75.5v57.1c0,12.2,9.2,22.6,21.4,24L73.6,160.8z M280.3,6.4c52,0,94.4,42.4,94.4,94.4s-42.4,94.4-94.4,94.4c-52,0-94.4-42.4-94.4-94.4S228.3,6.4,280.3,6.4z"><animate attributeName="stroke-dashoffset" begin="0s" values="2019; 0" dur="3s" repeatCount="1" fill="freeze" calcMode="linear"/><animate attributeName="stroke-dashoffset" begin="svg3.mouseover" values="2019; 0" dur="3s" repeatCount="1" fill="freeze" calcMode="linear"/></path><path stroke-dasharray="1632" stroke-dashoffset="0" d="M844.6,174.9l-30.1-25.4c-9.3-7.8-22.9-7.5-31.7,0.9l-16.6,15.6c-14.1-6.8-29-11.6-44.4-14.3l-4.7-22.9c-2.4-11.9-13.4-20.1-25.4-19.1l-39.3,3.3c-12,1.1-21.5,10.9-21.9,23l-0.8,23.3c-15.1,5.4-29.4,12.9-42.4,22.3l-19.9-13.2c-10.1-6.7-23.6-4.8-31.5,4.5l-25.4,30.3c-7.8,9.3-7.5,22.9,0.9,31.7l17.4,18.5c-6,13.6-10.3,27.8-12.8,42.4l-24.9,5.1c-11.9,2.4-20.1,13.4-19.1,25.4l3.3,39.3c1.1,12,10.9,21.5,23,21.9l26.9,0.9c4.9,12.8,11.3,25,19,36.3l-15,22.7c-6.7,10.1-4.8,23.6,4.5,31.5l30.1,25.4c9.3,7.8,22.9,7.5,31.7-0.9l19.7-18.5c12.9,6,26.5,10.5,40.4,13.2l5.5,27c2.4,11.9,13.4,20.1,25.4,19.1l39.3-3.3c12-1.1,21.5-10.9,21.9-23l0.9-26.4c14.3-5.1,27.9-12,40.4-20.5l21.7,14.3c10.1,6.7,23.6,4.8,31.5-4.5l25.4-30.1c7.8-9.3,7.5-22.9-0.9-31.7l-17.5-18.5c6.4-13.6,11.1-27.9,13.8-42.6l23.9-4.9c11.9-2.4,20.1-13.4,19.1-25.4l-3.3-39.3c-1.1-12-10.9-21.5-23-21.9l-24-0.8c-5-14-11.7-27.1-20-39.4l13.1-19.7C855.8,196.3,853.9,182.7,844.6,174.9z M696.5,402c-42.4,3.6-79.9-28-83.5-70.5c-3.6-42.4,28-79.9,70.5-83.5c42.4-3.6,79.9,28,83.5,70.5C770.6,361,738.9,398.5,696.5,402z"><animate attributeName="stroke-dashoffset" begin="0s" values="1632; 0" dur="3s" repeatCount="1" fill="freeze" calcMode="linear"/><animate attributeName="stroke-dashoffset" begin="svg3.mouseover" values="1632; 0" dur="3s" repeatCount="1" fill="freeze" calcMode="linear"/></path><path stroke-dasharray="1306" stroke-dashoffset="0" stroke="#AABF19" d="M211,507.8c-12,1.2-21.3,11.4-21.4,23.5l-0.3,24.2c-0.2,12.1,8.8,22.5,20.8,24l17.8,2.3c3,10.8,7.2,21.1,12.6,30.9l-11.5,14.1c-7.6,9.5-7.1,23,1.4,31.7l16.9,17.3c8.5,8.7,22.1,9.7,31.7,2.2l14.2-11c10,5.9,20.7,10.6,31.7,13.9l1.9,18.4c1.2,12,11.4,21.3,23.5,21.4l24.2,0.3c12.1,0.2,22.5-8.8,24-20.8l2.2-17.4c12-3,23.5-7.5,34.4-13.4l13.4,10.8c9.5,7.6,23,7.1,31.7-1.4l17.3-16.9c8.7-8.5,9.7-22.1,2.2-31.7l-10.2-13.3c6.3-10.5,11.2-21.7,14.5-33.5l15.9-1.6c12-1.2,21.3-11.4,21.4-23.5l0.3-24.2c0.2-12.1-8.8-22.5-20.8-24l-15.5-2c-3-11.7-7.4-22.9-13.1-33.5l9.7-11.9c7.6-9.5,7.1-23-1.4-31.7l-16.9-17.3c-8.5-8.7-22.1-9.7-31.7-2.2l-11.6,8.9c-10.9-6.6-22.6-11.7-34.8-15.1l-1.5-15c-1.2-12-11.4-21.3-23.5-21.4l-24.2-0.3c-12.1-0.2-22.5,8.8-24,20.8l-1.9,14.9c-12.6,3.2-24.8,8.2-36,14.7l-12-9.8c-9.5-7.6-23-7.1-31.7,1.4l-17.4,17c-8.7,8.5-9.7,22.1-2.2,31.7l10.2,13.2c-5.9,10.5-10.6,21.7-13.7,33.5L211,507.8z M367.1,480.4c34.7,0.4,62.5,29,62.1,63.7c-0.4,34.7-29,62.5-63.7,62.1c-34.7-0.4-62.5-29-62.1-63.7C303.9,507.9,332.4,480.1,367.1,480.4z"><animate attributeName="stroke-dashoffset" begin="0s" values="1306; 0" dur="3s" repeatCount="1" fill="freeze" calcMode="linear"/>.<animate attributeName="stroke-dashoffset" begin="svg3.mouseover" values="1306; 0" dur="3s" repeatCount="1" fill="freeze" calcMode="linear"/></path></g></svg>';
    };

    const handleIntersectionSvg = (entries, observerSvg) => {
      entries.forEach(entery => {
        if (entery.intersectionRatio > 0) {
          drawSvg(entery.target);
          observerSvg.unobserve(entery.target);
        }
      });
    };
    /*const isVisibleSVG = () => {
    	const topElPosition = elmYPosition('column_svg') + 200;
        const winPosition = document.getElementById('parallax-main-content').scrollTop + document.documentElement.clientHeight;
      	if (topElPosition < winPosition) {
            drawSvg();
            parallaxContainer.removeEventListener('scroll', isVisibleSVG, false);
        }
    };*/


    if ('IntersectionObserver' in window) {
      const svg = document.querySelector('#column_svg');
      const observerSvg = new IntersectionObserver(handleIntersectionSvg, optionsSvg);
      observerSvg.observe(svg);
    } else {
      //parallaxContainer.addEventListener('scroll', isVisibleSVG, false);
      const svg = document.querySelector('#column_svg');
      const observerSvg = new IntersectionObserver(handleIntersectionSvg, optionsSvg);
      observerSvg.POLL_INTERVAL = 100;
      observerSvg.observe(svg);
    } //set location window


    const optionsLocation = {
      root: document.querySelector('#parallax-main-content'),
      rootMargin: '0px',
      threshold: 1.0
    };

    const setLocation = url => {
      location.hash = url;
    };

    const handleIntersectionLocation = (entries, observerLocation) => {
      entries.forEach(entery => {
        if (entery.intersectionRatio > 0) {
          setLocation(entery.target.id);
        }
      });
    };

    if ('IntersectionObserver' in window) {
      const observerLocation = new IntersectionObserver(handleIntersectionLocation, optionsLocation);
      array.forEach(section => {
        observerLocation.observe(section);
      });
    }

    exports.array = array;
    return exports;
  }({});

}());
