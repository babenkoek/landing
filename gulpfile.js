const gulp = require('gulp');
const rollup = require('rollup');
const sass = require('gulp-sass');
const webp = require('gulp-webp');

gulp.task('build', () => {
	gulp.src('./src/html/*.html')
		.pipe(gulp.dest('./build'));

	gulp.src('./src/img/**/*')
		.pipe(gulp.dest('./build/img'));

	gulp.src('./src/fonts/**/*')
		.pipe(gulp.dest('./build/fonts'));
});

gulp.task('js', () => {
	return rollup.rollup({
		input: './src/js/main.js'
	}).then(bundl => {
		return bundl.write({
			file: './build/main.js',
			format: 'iife',
			name: 'main',
			sourcemap: true
		});
	});
});


gulp.task('css', () => {
	return gulp.src('./src/css/main.scss')
		.pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
		.pipe(gulp.dest('./build'));
});

gulp.task('webp', () => {
	gulp.src('./src/img/**/*')
		.pipe(webp())
		.pipe(gulp.dest('./build/img'));
});


